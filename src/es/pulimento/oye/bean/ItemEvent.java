package es.pulimento.oye.bean;

public abstract class ItemEvent {

	private int idItem;
	private long timestamp;

	public abstract String getField1();

	public abstract String getField2();

	public abstract String getField3();

	public abstract int getType();

	public int getIdItem() {
		return idItem;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setIdItem(int iditem) {
		this.idItem = iditem;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
}

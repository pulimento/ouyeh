package es.pulimento.oye.dao;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import es.pulimento.oye.bean.GeneratedActivity;
import es.pulimento.oye.db.ActivityTable;
import es.pulimento.oye.db.ItemTable;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.misc.Constants;

public class DAOActivity {

	public DAOActivity() {
	}

	public Future<Long> insert(final Context context,
			final GeneratedActivity act) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {
				long id = -1;

				ContentValues cvItem = new ContentValues();
				cvItem.put(ItemTable.COLUMN_TIMESTAMP,
						System.currentTimeMillis());
				cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_ACTIVITY);

				MainDatabaseHelper dbHelper = MainDatabaseHelper
						.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();

				long idItem = db
						.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
				ContentValues values = new ContentValues();
				values.put(ActivityTable.COLUMN_ID_ITEM, idItem);
				values.put(ActivityTable.COLUMN_ACTIVITY, act.getActivity());
				values.put(ActivityTable.COLUMN_NAME, act.getName());
				values.put(ActivityTable.COLUMN_PERSON, act.getPerson());
				id = db.insert(ActivityTable.TABLE_NAME, "<empty>", values);

				//db.close();

				return id;
			};
		};
		ScheduledExecutorService s = Executors
				.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

	public Future<Long> getNewerTimestamp(final Context context) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper
						.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				long newerTimestamp = -1;

				Cursor c = db
						.query(ItemTable.TABLE_NAME,
								new String[] { ItemTable.COLUMN_TIMESTAMP },
								"type=?", new String[] { String
										.valueOf(Constants.TYPE_ACTIVITY) },
								null, null, ItemTable.COLUMN_TIMESTAMP
										+ " DESC", "1");

				if (c.moveToFirst()) {
					newerTimestamp = c.getLong(0);// Only one column
				}

				//db.close();

				return newerTimestamp;
			};
		};
		ScheduledExecutorService s = Executors
				.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

}

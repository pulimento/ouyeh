package es.pulimento.oye.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import es.pulimento.oye.misc.BaseReceiver;

public class CameraReceiver extends BaseReceiver {

	private static final String OWN_NAME = "CameraReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();

		if (null == bundle)
			return;

		potter(context, intent, OWN_NAME, true, null);
	}
}
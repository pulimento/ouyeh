package es.pulimento.oye;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import es.pulimento.oye.bean.User;
import es.pulimento.oye.dao.DAOUser;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.db.MockTable;
import es.pulimento.oye.thirdpartyapis.twitter.TwitterHandlerActivity;

public class MainActivity extends Activity implements OnClickListener {

	// Esta actividad simplemente se debe encargar de hacer las peticiones
	// (consumir la API interna basada en Intents) y procesar las respuestas
	// (callbacks)

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Start always running service
		Intent service = new Intent(MainActivity.this,
				AlwaysRunningService.class);
		if (!isMyServiceRunning())
			startService(service);

		((Button) findViewById(R.id.button1)).setOnClickListener(this);
		((Button) findViewById(R.id.button2)).setOnClickListener(this);
		((Button) findViewById(R.id.btn_main_twitter)).setOnClickListener(this);
		((Button) findViewById(R.id.btn_main_mocks)).setOnClickListener(this);
		((Button) findViewById(R.id.btn_main_tl)).setOnClickListener(this);

		DAOUser d = new DAOUser(MainActivity.this);
		User u = d.getUser();
		
		ImageView userPhoto = (ImageView) findViewById(R.id.iv_main_picture);
		userPhoto.setImageDrawable(getResources().getDrawable(
				R.drawable.placeholder_image));
		if (u != null) {
			UrlImageViewHelper.setUrlDrawable(userPhoto, u.getPicture());
			((TextView) findViewById(R.id.tv_main_hello)).setText("Hey "
					+ u.getGiven_name() + ", un placer verte de nuevo!");
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button1:
			// intent = new Intent(MainActivity.this, InsertPDVActivity.class);
			// startActivity(intent);
			SQLiteDatabase db = MainDatabaseHelper.getInstance(
					MainActivity.this).getWritableDatabase();
			ContentValues cv = new ContentValues();
			cv.put(MockTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
			cv.put(MockTable.COLUMN_INTENT, "SIMULATED INTENT");
			cv.put(MockTable.COLUMN_DATA_1, "ola k ase");
			db.insertOrThrow(MockTable.TABLE_MOCK, "nch", cv);
			db.close();
			Toast.makeText(MainActivity.this, "intent simulated and inserted",
					Toast.LENGTH_SHORT).show();
			break;
		case R.id.button2:
			// intent = new Intent(MainActivity.this, HolderActivity.class);
			// startActivity(intent);
			Intent service = new Intent(MainActivity.this,
					AlwaysRunningService.class);
			startService(service);
			break;
		case R.id.btn_main_mocks:
			Intent intent2 = new Intent(MainActivity.this, MocksActivity.class);
			startActivity(intent2);
			break;
		case R.id.btn_main_twitter:
			Intent intent3 = new Intent(MainActivity.this,
					TwitterHandlerActivity.class);
			startActivity(intent3);
			break;
		case R.id.btn_main_tl:
			Intent intent4 = new Intent(MainActivity.this,
					TimelineActivity.class);
			startActivity(intent4);
			break;
		}
	}

	boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager
				.getRunningServices(Integer.MAX_VALUE)) {
			if (AlwaysRunningService.class.getName().equals(
					service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}

package es.pulimento.oye.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import es.pulimento.oye.misc.BaseReceiver;

public class PhoneStateReceiver extends BaseReceiver {

	private static final String OWN_NAME = "Phone state";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();

		if (null == bundle)
			return;

		String status = "";
		long time = System.currentTimeMillis();

		if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
				TelephonyManager.EXTRA_STATE_RINGING)) {
			status += "RINGING";
		}
		if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
				TelephonyManager.EXTRA_STATE_OFFHOOK)) {
			status += "OFFHOOK";
		}
		if (intent.getStringExtra(TelephonyManager.EXTRA_STATE).equals(
				TelephonyManager.EXTRA_STATE_IDLE)) {
			status += "IDLE";
		}
		potter(context, intent, OWN_NAME, true, "STATUS-> " + status
				+ " ,TIMESTAMP-> " + String.valueOf(time));
	}
}
package es.pulimento.oye.misc;

import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.db.MockTable;

public abstract class BaseReceiver extends BroadcastReceiver {

	public boolean potter(Context context, Intent intent, String OWN_NAME, boolean potExtras,
			String data2) {
		try {
			Bundle bundle = intent.getExtras();
			String intentExtras = "";
			ContentValues cv = new ContentValues();

			Log.i(Constants.TAG, "Hitted receiver " + OWN_NAME + "(" + intent.getAction()
					+ "),START");

			if (potExtras) {
				for (String key : bundle.keySet()) {
					Object value = bundle.get(key);
					intentExtras += intent.getAction();
					Log.v(Constants.TAG, "key -> " + key + ", value -> " + value.toString());
					intentExtras += "<" + key + " : " + value.toString() + ">";
				}
				cv.put(MockTable.COLUMN_DATA_1, intentExtras);
			}

			cv.put(MockTable.COLUMN_DATA_2, data2);

			Log.i(Constants.TAG, OWN_NAME + " END");

			SQLiteDatabase db = MainDatabaseHelper.getInstance(context).getWritableDatabase();
			cv.put(MockTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
			cv.put(MockTable.COLUMN_INTENT, OWN_NAME);

			db.insertOrThrow(MockTable.TABLE_MOCK, "nch", cv);
			db.close();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

}

package es.pulimento.oye;

import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.common.GooglePlayServicesUtil;

import es.pulimento.oye.bean.User;
import es.pulimento.oye.dao.DAOUser;
import es.pulimento.oye.misc.Constants;
import es.pulimento.oye.thirdpartyapis.google.GetNameInForeground;

public class InitActivity extends Activity {

	private AccountManager mAccountManager;

	static final int REQUEST_CODE_RECOVER_FROM_AUTH_ERROR = 1001;
	static final int REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 1002;

	private String[] mNamesArray;
	private String mEmail;

	@Override
	protected void onCreate(Bundle savedInstanceState) {		
		setContentView(R.layout.activity_init);	
		
		final ActionBar actionBar = getActionBar();
		loadFakeActionBar(actionBar);
		
		super.onCreate(savedInstanceState);

		boolean googleUserYetfetched = false;

		DAOUser d = new DAOUser(this);
		int numOfUsers = d.getUsersCount();
		if (numOfUsers > 0) {
			googleUserYetfetched = true;
			SharedPreferences settings = PreferenceManager
					.getDefaultSharedPreferences(this);
			SharedPreferences.Editor prefEditor = settings.edit();
			// Save login status
			if (d.isLoggedOnTwitter())
				prefEditor.putBoolean(Constants.PREFS_IS_LOGGED_IN_TWITTER,
						true);
			else
				prefEditor.putBoolean(Constants.PREFS_IS_LOGGED_IN_TWITTER,
						false);
			prefEditor.commit();
		}

		if (!googleUserYetfetched) {
			Log.i(Constants.TAG,
					"There isn't existing users in db, hitting Google servers...");
			mNamesArray = getAccountNames();

			if (mNamesArray.length < 1) {
				// this happens when the sample is run in an emulator which
				// has no google account
				// added yet.
				show("No account available. Please add an account to the phone first.");
				return;
			}
			mEmail = mNamesArray[0];
			new GetNameInForeground(InitActivity.this, mEmail,
					Constants.GOOGLE_OAUTH_SCOPE,
					REQUEST_CODE_RECOVER_FROM_AUTH_ERROR).execute();
		} else {
			Log.i(Constants.TAG,
					"There is an existing user in db, not hitting Google servers...");
			Intent intent = new Intent(InitActivity.this,
					TimelineActivity.class);

			startActivity(intent);
			InitActivity.this.finish();
		}

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUEST_CODE_RECOVER_FROM_AUTH_ERROR) {
			handleAuthorizeResult(resultCode, data);
			return;
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * This method is a hook for background threads and async tasks that need to
	 * update the UI. It does this by launching a runnable under the UI thread.
	 */
	public void show(final String message) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Log.i(Constants.TAG, "Json potted by Google -> " + message);
				User u = null;
				try {
					JSONObject json = new JSONObject(message);
					u = new User(null, json.getString("id"),
							json.getString("given_name"),
							json.getString("family_name"),
							json.optString("picture"), json.getString("gender")
									.equals("male") ? 1 : 0,
							json.getString("locale"));
				} catch (JSONException e) {
					e.printStackTrace();
				}
				DAOUser d = new DAOUser(InitActivity.this);
				d.insertFromGoogle(u);
				Intent intent = new Intent(InitActivity.this,
						TimelineActivity.class);
				intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
				startActivityForResult(intent, 0);
				overridePendingTransition(0,0); //0 for no animation
				InitActivity.this.finish();
			}
		});
	}

	/**
	 * This method is a hook for background threads and async tasks that need to
	 * launch a dialog. It does this by launching a runnable under the UI
	 * thread.
	 */
	public void showErrorDialog(final int code) {
		runOnUiThread(new Runnable() {
			@Override
			public void run() {
				Dialog d = GooglePlayServicesUtil.getErrorDialog(code,
						InitActivity.this,
						REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
				d.show();
			}
		});
	}

	private void handleAuthorizeResult(int resultCode, Intent data) {
		if (data == null) {
			show("Unknown error, click the button again");
			return;
		}
		if (resultCode == RESULT_OK) {
			Log.i(Constants.TAG, "Retrying");
			new GetNameInForeground(this, mEmail, Constants.GOOGLE_OAUTH_SCOPE,
					REQUEST_CODE_RECOVER_FROM_AUTH_ERROR).execute();
			return;
		}
		if (resultCode == RESULT_CANCELED) {
			show("User rejected authorization.");
			return;
		}
		show("Unknown error, click the button again");
	}

	private String[] getAccountNames() {
		mAccountManager = AccountManager.get(this);
		Account[] accounts = mAccountManager
				.getAccountsByType(GoogleAuthUtil.GOOGLE_ACCOUNT_TYPE);
		String[] names = new String[accounts.length];
		for (int i = 0; i < names.length; i++) {
			names[i] = accounts[i].name;
		}
		return names;
	}

	private void loadFakeActionBar(final ActionBar actionBar) {
		// Define custom action bar...
		//FrameLayout ll = new FrameLayout(this);
		//ll.setBackgroundColor(getResources().getColor(R.color.ab_background));
		//actionBar.setCustomView(ll);
		//actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

	}

}
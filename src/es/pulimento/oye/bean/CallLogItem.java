package es.pulimento.oye.bean;

import es.pulimento.oye.misc.Constants;

public class CallLogItem extends ItemEvent {

	private int _id;
	private String name;
	private String number;
	private String durationString;
	private int callType;
	private long callTimestamp;
	public static final int type = Constants.TYPE_CALL_LOG;
	
	public static final int TYPE_INCOMING = 1;
	public static final int TYPE_OUTGOING = 2;
	public static final int TYPE_MISSED = 3;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getDurationString() {
		return durationString;
	}

	public void setDurationString(String durationString) {
		this.durationString = durationString;
	}

	public int getCallType() {
		return callType;
	}

	public void setCallType(int callType) {
		this.callType = callType;
	}

	public long getCallTimestamp() {
		return callTimestamp;
	}

	public void setCallTimestamp(long callTimestamp) {
		this.callTimestamp = callTimestamp;
	}

	public int get_id() {
		return _id;
	}

	@Override
	public int getType() {
		return type;
	}

	@Override
	public String getField1() {
		return name;
	}

	@Override
	public String getField2() {
		return number;
	}

	@Override
	public String getField3() {
		// TODO Auto-generated method stub
		return null;
	}

}

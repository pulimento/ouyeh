package es.pulimento.oye.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;
import es.pulimento.oye.BuildConfig;
import es.pulimento.oye.misc.BaseReceiver;
import es.pulimento.oye.misc.Constants;

public class OutgoingCallReceiver extends BaseReceiver {

	private static final String OWN_NAME = "Outgoing call";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();

		if (null == bundle)
			return;

		String phonenumber = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

		if (BuildConfig.DEBUG) {
			String info = "Llamada saliente - " + phonenumber;
			Log.i(Constants.TAG, info);
			Toast.makeText(context, info, Toast.LENGTH_LONG).show();
		}

		potter(context, intent, OWN_NAME, false, phonenumber);
	}
}
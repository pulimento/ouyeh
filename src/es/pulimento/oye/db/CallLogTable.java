package es.pulimento.oye.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import es.pulimento.oye.misc.Constants;

public class CallLogTable {

	// Database table
	public static final String TABLE_NAME = "call_log";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_ID_ITEM = "iditem";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_NUMBER = "number";
	public static final String COLUMN_FETCHED_AT = "fetched_at";
	public static final String COLUMN_DURATION = "duration";
	public static final String COLUMN_CALL_TYPE = "calltype";

	// Database creation SQLite statement
	public static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
			+ COLUMN_ID + " integer primary key autoincrement," + COLUMN_NAME + " text, "
			+ COLUMN_CALL_TYPE + " integer," + COLUMN_NUMBER + " text," + COLUMN_DURATION
			+ " text," + COLUMN_FETCHED_AT + " integer," + COLUMN_ID_ITEM + " integer);";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		Log.w(Constants.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
	}

}

package es.pulimento.oye.misc.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewRobotoSlab extends TextView {

	public TextViewRobotoSlab(Context context) {
		super(context);
		useCustomTypography();
	}

	public TextViewRobotoSlab(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		useCustomTypography();
	}

	public TextViewRobotoSlab(Context context, AttributeSet attrs) {
		super(context, attrs);
		useCustomTypography();
	}

	private void useCustomTypography() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"fonts/RobotoSlab-Regular.ttf");
		setTypeface(tf, 1);
	}
}

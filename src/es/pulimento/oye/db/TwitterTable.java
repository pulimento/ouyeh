package es.pulimento.oye.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import es.pulimento.oye.misc.Constants;

public class TwitterTable {

	// Database table
	public static final String TABLE_NAME = "twitter";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_ID_ITEM = "iditem";
	public static final String COLUMN_FETCHED_AT = "fetched_at";
	public static final String COLUMN_GEOLOCATION = "geolocation";
	public static final String COLUMN_PLACE = "place";
	public static final String COLUMN_RETWEET_COUNT = "rt_count";
	public static final String COLUMN_SOURCE = "source";
	public static final String COLUMN_TEXT = "text";
	public static final String COLUMN_USER = "user";
	public static final String COLUMN_TWITTER_ID = "twitterid";
	public static final String COLUMN_TWEET_TYPE = "tweet_type";

	// Database creation SQLite statement
	public static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS " + TABLE_NAME + "("
			+ COLUMN_ID + " integer primary key autoincrement," + COLUMN_FETCHED_AT + " integer, "
			+ COLUMN_GEOLOCATION + " text," + COLUMN_PLACE + " text," + COLUMN_RETWEET_COUNT
			+ " integer," + COLUMN_SOURCE + " text," + COLUMN_TEXT + " text," + COLUMN_TWITTER_ID
			+ " integer," + COLUMN_TWEET_TYPE + " integer," + COLUMN_USER + " text,"
			+ COLUMN_ID_ITEM + " integer);";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		Log.w(Constants.TAG, "Upgrading database from version " + oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
	}

}

package es.pulimento.oye;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import es.pulimento.oye.bean.CommentItem;
import es.pulimento.oye.dao.DAOComment;

public class CreateCommentActivity extends Activity implements OnClickListener,
		TextWatcher {

	private EditText mEtContent;
	private FrameLayout mBtnSend;
	private TextView mTvCharsRemaining;
	private ImageView mBtnClose;

	private static final int MAX_CHARS = 200;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_comment);

		setFinishOnTouchOutside(false);

		mEtContent = (EditText) findViewById(R.id.et_createcomment_text);
		mBtnSend = (FrameLayout) findViewById(R.id.btn_createcomment_send);
		mTvCharsRemaining = (TextView) findViewById(R.id.tv_createcomment_charsremaining);
		mBtnClose = (ImageView) findViewById(R.id.btn_createcomment_close);

		mBtnSend.setOnClickListener(this);
		mBtnClose.setOnClickListener(this);
		mEtContent.addTextChangedListener(this);
		mTvCharsRemaining.setText(String.valueOf(MAX_CHARS));
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_createcomment_send:
			sendCommentItem(mEtContent.getText().toString());
			break;
		case R.id.btn_createcomment_close:
			break;// It goes to hell
		}
		this.finish();
	}

	// ///////////////////////////////////////////////////////////////////////////////
	// TextWatcher stuff, shows remaining characters
	// ///////////////////////////////////////////////////////////////////////////////

	@Override
	public void afterTextChanged(Editable s) {
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count,
			int after) {
		// Seems to do nothing...
		// mTvCharsRemaining.setText(String.valueOf(mMaxChars));
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
		String remaining = String.valueOf(MAX_CHARS
				- mEtContent.getText().length());
		mTvCharsRemaining.setText(remaining);
	}

	private void sendCommentItem(String userComment) {

		if (userComment != null) {
			DAOComment d = new DAOComment();
			CommentItem c = new CommentItem();
			c.setText(userComment);
			c.setMisc("Hardcoded");
			d.insert(CreateCommentActivity.this, c);
		} else {
			Crouton.makeText(CreateCommentActivity.this,
					"No hay nada escrito!!!", Style.ALERT).show();
		}
	}

}
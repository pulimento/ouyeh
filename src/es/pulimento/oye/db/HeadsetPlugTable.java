package es.pulimento.oye.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import es.pulimento.oye.misc.Constants;

public class HeadsetPlugTable {

	// Database table
	public static final String TABLE_NAME = "headset_plug";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_ID_ITEM = "iditem";
	public static final String COLUMN_STATE = "state";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_MICROPHONE = "microphone";
	public static final String[] AVAILABLE_COLUMNS = { COLUMN_ID,
			COLUMN_ID_ITEM, COLUMN_STATE, COLUMN_NAME, COLUMN_MICROPHONE };

	// Database creation SQLite statement
	public static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME + "(" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_STATE
			+ " integer," + COLUMN_NAME + " text," + COLUMN_MICROPHONE
			+ " integer," + COLUMN_ID_ITEM + " integer);";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(Constants.TAG, "Upgrading database from version " + oldVersion
				+ " to " + newVersion + ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
	}

}

package es.pulimento.oye.misc;

import java.util.regex.Pattern;

public class Constants {

	public static final String TAG = "ouyeah";
	public static final String PACKAGE_NAME = "es.pulimento.oye";
	public static final boolean SWIPELISTVIEW_DEBUG = false;

	// Google login
	// public static final String GOOGLE_OAUTH_SCOPE =
	// "oauth2:https://www.googleapis.com/auth/userinfo.profile oauth2:https://www.googleapis.com/auth/plus.me oauth2:https://www.googleapis.com/auth/plus.login";
	public static final String GOOGLE_OAUTH_SCOPE = "oauth2:https://www.googleapis.com/auth/userinfo.profile";

	// Twitter API
	public static final String TWITTER_CONSUMER_KEY = "fLNIOcv1Woz2QS8uNvCHIg";
	public static final String TWITTER_CONSUMER_SECRET = "MMkaHhebKVFrtk2yHs99PkIzwki32iEmlJ4S2pxKM";
	public static final String TWITTER_ACCESS_TOKEN = "16915767-xLW2x6IGqroJEWzIVbrbILuqoikcBF2TY2kebFAi0";
	public static final String TWITTER_ACCESS_TOKEN_SECRET = "wABFKxjcsQPbUeQHvSErUHW3n8bMqzy5eBIimI6GKL0";
	public static final String PREFS_TWITTER_ACCESS_TOKEN = "twitteraccesstoken";
	public static final String PREFS_TWITTER_ACCESS_TOKEN_SECRET = "twitteraccesstokensecret";
	public static final String PREFS_TWITTER_USERNAME = "twitterusername";
	public static final int INTENT_SIGN_IN_TWITTER = 0x100001;
	public static final String SHARE_TWITTER_HASHTAG = "#OYE";

	// Bean types
	public static final int TYPE_HEADSET_PLUG = 0x7700;
	public static final int VIEW_HEADSET_PLUG = 0;
	public static final int TYPE_MOCK_1 = 0x7710;
	public static final int TYPE_ACTIVITY = 0x7720;
	public static final int VIEW_ACTIVITY = 1;
	public static final int TYPE_POSITION = 0x7730;
	public static final int VIEW_POSITION = 2;
	public static final int TYPE_CALL_LOG = 0x7740;
	public static final int VIEW_CALL_LOG = 3;
	public static final int TYPE_TWITTER = 0x7750;
	public static final int VIEW_TWITTER = 4;
	public static final int TYPE_COMMENT = 0x7760;
	public static final int VIEW_COMMENT = 5;
	public static final int TYPE_SONG_PLAYED = 0x7770;
	public static final int VIEW_SONG_PLAYED = 6;

	// Item provider provisional flags
	public static final boolean SHOW_HEADSET_PLUG = false;
	public static final boolean SHOW_ACTIVITY = true;
	public static final boolean SHOW_POSITION = false;
	public static final boolean SHOW_CALL_LOG = true;
	public static final boolean SHOW_TWITTER = true;
	public static final boolean SHOW_COMMENT = true;
	
	//Right panel
	public static final String PREFS_IS_LOGGED_IN_TWITTER = "isloggedintwitter";

	// Final provider flags
	public static final String PREFS_KEY_SHOW_POSITION = "showposition";
	public static final String PREFS_KEY_SHOW_TWITTER = "showtwitter";
	public static final String PREFS_KEY_SHOW_SONG_PLAYED = "showsongplayed";
	public static final String PREFS_KEY_SHOW_CALL_LOG = "showcalllog";
	public static final String PREFS_KEY_SHOW_ACTIVITY = "showactivity";

	// Twitter word black list
	public static final Pattern[] PATTERNS_TWITTER_WORDS = new Pattern[] {
			Pattern.compile("asi", Pattern.CASE_INSENSITIVE),
			Pattern.compile("ahora", Pattern.CASE_INSENSITIVE),
			Pattern.compile("as�", Pattern.CASE_INSENSITIVE),
			Pattern.compile("and", Pattern.CASE_INSENSITIVE),
			Pattern.compile("aquel", Pattern.CASE_INSENSITIVE),
			Pattern.compile("algo", Pattern.CASE_INSENSITIVE),
			Pattern.compile("ahi", Pattern.CASE_INSENSITIVE),
			Pattern.compile("ah�", Pattern.CASE_INSENSITIVE),
			Pattern.compile("aqui", Pattern.CASE_INSENSITIVE),
			Pattern.compile("aqu�", Pattern.CASE_INSENSITIVE),
			Pattern.compile("are", Pattern.CASE_INSENSITIVE),
			Pattern.compile("cada", Pattern.CASE_INSENSITIVE),
			Pattern.compile("con", Pattern.CASE_INSENSITIVE),
			Pattern.compile("can", Pattern.CASE_INSENSITIVE),
			Pattern.compile("como", Pattern.CASE_INSENSITIVE),
			Pattern.compile("del", Pattern.CASE_INSENSITIVE),
			Pattern.compile("dia", Pattern.CASE_INSENSITIVE),
			Pattern.compile("d�a", Pattern.CASE_INSENSITIVE),
			Pattern.compile("donde", Pattern.CASE_INSENSITIVE),
			Pattern.compile("dos", Pattern.CASE_INSENSITIVE),
			Pattern.compile("eres", Pattern.CASE_INSENSITIVE),
			Pattern.compile("este", Pattern.CASE_INSENSITIVE),
			Pattern.compile("eso", Pattern.CASE_INSENSITIVE),
			Pattern.compile("esto", Pattern.CASE_INSENSITIVE),
			Pattern.compile("esta", Pattern.CASE_INSENSITIVE),
			Pattern.compile("est�", Pattern.CASE_INSENSITIVE),
			Pattern.compile("for", Pattern.CASE_INSENSITIVE),
			Pattern.compile("from", Pattern.CASE_INSENSITIVE),
			Pattern.compile("gran", Pattern.CASE_INSENSITIVE),
			Pattern.compile("has", Pattern.CASE_INSENSITIVE),
			Pattern.compile("han", Pattern.CASE_INSENSITIVE),
			Pattern.compile("hoy", Pattern.CASE_INSENSITIVE),
			Pattern.compile("hay", Pattern.CASE_INSENSITIVE),
			Pattern.compile("haz", Pattern.CASE_INSENSITIVE),
			Pattern.compile("http", Pattern.CASE_INSENSITIVE),
			Pattern.compile("hasta", Pattern.CASE_INSENSITIVE),
			Pattern.compile("haga", Pattern.CASE_INSENSITIVE),
			Pattern.compile("ido", Pattern.CASE_INSENSITIVE),
			Pattern.compile("jeje", Pattern.CASE_INSENSITIVE),
			Pattern.compile("jejeje", Pattern.CASE_INSENSITIVE),
			Pattern.compile("jaja", Pattern.CASE_INSENSITIVE),
			Pattern.compile("jajaja", Pattern.CASE_INSENSITIVE),
			Pattern.compile("los", Pattern.CASE_INSENSITIVE),
			Pattern.compile("las", Pattern.CASE_INSENSITIVE),
			Pattern.compile("mas", Pattern.CASE_INSENSITIVE),
			Pattern.compile("m�s", Pattern.CASE_INSENSITIVE),
			Pattern.compile("muy", Pattern.CASE_INSENSITIVE),
			Pattern.compile("nos", Pattern.CASE_INSENSITIVE),
			Pattern.compile("now", Pattern.CASE_INSENSITIVE),
			Pattern.compile("nada", Pattern.CASE_INSENSITIVE),
			Pattern.compile("out", Pattern.CASE_INSENSITIVE),
			Pattern.compile("our", Pattern.CASE_INSENSITIVE),
			Pattern.compile("one", Pattern.CASE_INSENSITIVE),
			Pattern.compile("para", Pattern.CASE_INSENSITIVE),
			Pattern.compile("pero", Pattern.CASE_INSENSITIVE),
			Pattern.compile("por", Pattern.CASE_INSENSITIVE),
			Pattern.compile("que", Pattern.CASE_INSENSITIVE),
			Pattern.compile("qu�", Pattern.CASE_INSENSITIVE),
			Pattern.compile("quiere", Pattern.CASE_INSENSITIVE),
			Pattern.compile("quien", Pattern.CASE_INSENSITIVE),
			Pattern.compile("soy", Pattern.CASE_INSENSITIVE),
			Pattern.compile("the", Pattern.CASE_INSENSITIVE),
			Pattern.compile("tal", Pattern.CASE_INSENSITIVE),
			Pattern.compile("tus", Pattern.CASE_INSENSITIVE),
			Pattern.compile("todos", Pattern.CASE_INSENSITIVE),
			Pattern.compile("todo", Pattern.CASE_INSENSITIVE),
			Pattern.compile("una", Pattern.CASE_INSENSITIVE),
			Pattern.compile("vas", Pattern.CASE_INSENSITIVE),
			Pattern.compile("ver", Pattern.CASE_INSENSITIVE),
			Pattern.compile("vaya", Pattern.CASE_INSENSITIVE),
			Pattern.compile("via", Pattern.CASE_INSENSITIVE),
			Pattern.compile("v�a", Pattern.CASE_INSENSITIVE),
			Pattern.compile("who", Pattern.CASE_INSENSITIVE) };
}

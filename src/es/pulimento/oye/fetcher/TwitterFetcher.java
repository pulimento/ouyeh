package es.pulimento.oye.fetcher;

import java.util.concurrent.ExecutionException;

import twitter4j.Paging;
import twitter4j.ResponseList;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.util.Log;
import es.pulimento.oye.R;
import es.pulimento.oye.dao.DAOTwitter;
import es.pulimento.oye.misc.Constants;

public class TwitterFetcher {

	private ResponseList<Status> statuses;
	private DAOTwitter d;
	RequestToken rt;
	private SharedPreferences settings;
	private Twitter twitter;

	public TwitterFetcher(Context context) {

		d = new DAOTwitter();

		settings = PreferenceManager.getDefaultSharedPreferences(context);

		// The factory instance is re-useable and thread safe.
		ConfigurationBuilder cb = new ConfigurationBuilder();
		cb.setGZIPEnabled(true);
		cb.setIncludeRTsEnabled(true);
		cb.setIncludeMyRetweetEnabled(true);
		cb.setOAuthConsumerKey(context.getString(R.string.twitter_consumer_key_oye));
		cb.setOAuthConsumerSecret(context.getString(R.string.twitter_consumer_secret_oye));

		twitter = new TwitterFactory(cb.build()).getInstance();

		// Add user access token
		String at = settings.getString(Constants.PREFS_TWITTER_ACCESS_TOKEN, "");
		String ats = settings.getString(Constants.PREFS_TWITTER_ACCESS_TOKEN_SECRET, "");

		AccessToken a = new AccessToken(at, ats);
		twitter.setOAuthAccessToken(a);
	}

	public void defaultFetchToDB(final Context context) {
		new AsyncTask<Void, Void, Void>() {

			@Override
			protected Void doInBackground(Void... params) {
				long lastId = -1;
				try {
					lastId = d.getNewerTweet(context).get();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				} catch (ExecutionException e1) {
					e1.printStackTrace();
				}
				try {
					if (lastId != -1) {
						Paging p = new Paging(lastId);

						statuses = twitter.getUserTimeline(p);
						statuses.addAll(twitter.getMentionsTimeline(p));
						// statuses.addAll(twitter.getRetweetsOfMe(p));
						// statuses.addAll(twitter.getHomeTimeline(p));
					} else {
						statuses = twitter.getUserTimeline();
						statuses.addAll(twitter.getMentionsTimeline());
						// statuses.addAll(twitter.getRetweetsOfMe());
						// statuses.addAll(twitter.getHomeTimeline());
					}

				} catch (TwitterException e) {
					e.printStackTrace();
				}

				Log.i(Constants.TAG, "tweets received: " + statuses.size());
				d.insert(context, statuses);
				showUserTimelinesOnLogcat(statuses);
				return null;
			}
		}.execute();
		// TODO execute on executor
	}

	public void showUserTimelinesOnLogcat(ResponseList<Status> statuses) {
		for (Status status : statuses)
			System.out.println(status.getUser().getName() + ":" + status.getText());
	}

}

package es.pulimento.oye.bean;

import es.pulimento.oye.misc.Constants;

public class GeneratedActivity extends ItemEvent {

	private int _id;
	private String activity;
	private String name;
	private String person;
	public static final int type = Constants.TYPE_ACTIVITY;

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getActivity() {
		return activity;
	}

	public void setActivity(String activity) {
		this.activity = activity;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPerson() {
		return person;
	}

	public void setPerson(String person) {
		this.person = person;
	}

	public int getType() {
		return GeneratedActivity.type;
	}

	@Override
	public String getField1() {
		return activity;
	}

	@Override
	public String getField2() {
		return person;
	}

	@Override
	public String getField3() {
		return "hey";
	}

}

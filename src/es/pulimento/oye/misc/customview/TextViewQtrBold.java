package es.pulimento.oye.misc.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewQtrBold extends TextView {

	public TextViewQtrBold(Context context) {
		super(context);
		useCustomTypography();
	}

	public TextViewQtrBold(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		useCustomTypography();
	}

	public TextViewQtrBold(Context context, AttributeSet attrs) {
		super(context, attrs);
		useCustomTypography();
	}

	private void useCustomTypography() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"fonts/QuattrocentoSans-Bold.ttf");
		setTypeface(tf, 1);
	}
}

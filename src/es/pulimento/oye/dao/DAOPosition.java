package es.pulimento.oye.dao;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import es.pulimento.oye.bean.PositionEvent;
import es.pulimento.oye.db.ItemTable;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.db.PositionTable;
import es.pulimento.oye.misc.Constants;

public class DAOPosition {

	public DAOPosition() {
	}

	public Future<Long> insert(final Context context, final ContentValues values) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {
				long id = -1;

				ContentValues cvItem = new ContentValues();
				cvItem.put(ItemTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
				cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_POSITION);

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				// db.beginTransaction();
				long idItem = db.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
				values.put(PositionTable.COLUMN_ID_ITEM, idItem);
				id = db.insert(PositionTable.TABLE_NAME, "<empty>", values);
				// db.setTransactionSuccessful();
				// db.endTransaction();
				// db.close();

				return id;
			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

	public Future<Set<PositionEvent>> getAllPositionEvents(final Context context) {

		Callable<Set<PositionEvent>> c = new Callable<Set<PositionEvent>>() {

			@Override
			public Set<PositionEvent> call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				Set<PositionEvent> items = new HashSet<PositionEvent>();
				DecimalFormat df = new DecimalFormat("#.##", new DecimalFormatSymbols(
						Locale.UK));

				SQLiteDatabase db = dbHelper.getWritableDatabase();

				Cursor cursor = db.query(PositionTable.TABLE_NAME, new String[] {
						PositionTable.COLUMN_LATITUDE, PositionTable.COLUMN_LONGITUDE },
						null, null, null, null, null);

				while (cursor.moveToNext()) {
					PositionEvent p = new PositionEvent();
					double latitude = cursor.getDouble(cursor
							.getColumnIndexOrThrow(PositionTable.COLUMN_LATITUDE));
					double longitude = cursor.getDouble(cursor
							.getColumnIndexOrThrow(PositionTable.COLUMN_LONGITUDE));
					p.setLatitude(latitude);
					p.setLongitude(longitude);
					p.roundDecimals(df);
					items.add(p);
				}

				Log.i(Constants.TAG, "Returned " + items.size() + " positions");

				return items;

			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

}

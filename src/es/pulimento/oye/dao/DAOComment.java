package es.pulimento.oye.dao;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import es.pulimento.oye.bean.CommentItem;
import es.pulimento.oye.db.CommentTable;
import es.pulimento.oye.db.ItemTable;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.misc.Constants;

public class DAOComment {

	public DAOComment() {
	}

	// public Future<Long> insert(final Context context, final ContentValues
	// values) {
	//
	// Callable<Long> c = new Callable<Long>() {
	//
	// @Override
	// public Long call() throws Exception {
	// long id = -1;
	//
	// ContentValues cvItem = new ContentValues();
	// cvItem.put(ItemTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
	// cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_CALL_LOG);
	//
	// MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
	// SQLiteDatabase db = dbHelper.getWritableDatabase();
	//
	// long idItem = db.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
	// values.put(HeadsetPlugTable.COLUMN_ID_ITEM, idItem);
	// id = db.insert(HeadsetPlugTable.TABLE_NAME, "<empty>", values);
	//
	// db.close();
	//
	// return id;
	// };
	// };
	// ScheduledExecutorService s =
	// Executors.newSingleThreadScheduledExecutor();
	// return s.submit(c);
	// }
	//
	public Future<Long> insert(final Context context, final CommentItem comment) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {
				long id = -1;

				ContentValues cvItem = new ContentValues();
				cvItem.put(ItemTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
				cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_COMMENT);

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();

				long idItem = db.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
				ContentValues values = new ContentValues();
				values.put(CommentTable.COLUMN_ID_ITEM, idItem);
				values.put(CommentTable.COLUMN_NAME, comment.getText());
				values.put(CommentTable.COLUMN_IMAGE_NAME, comment.getImageName());
				values.put(CommentTable.COLUMN_MISC, comment.getMisc());
				id = db.insert(CommentTable.TABLE_NAME, "<empty>", values);

				//db.close();

				return id;
			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

	public Future<Long> getNewerTimestamp(final Context context) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				long newerTimestamp = -1;

				Cursor c = db.query(ItemTable.TABLE_NAME,
						new String[] { ItemTable.COLUMN_TIMESTAMP }, "type=?",
						new String[] { String.valueOf(Constants.TYPE_COMMENT) }, null,
						null, ItemTable.COLUMN_TIMESTAMP + " DESC", "1");

				if (c.moveToFirst()) {
					newerTimestamp = c.getLong(0);// Only one column
				}

				//db.close();

				return newerTimestamp;
			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

}

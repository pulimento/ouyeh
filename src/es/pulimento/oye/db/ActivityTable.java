package es.pulimento.oye.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import es.pulimento.oye.misc.Constants;

public class ActivityTable {

	// Database table
	public static final String TABLE_NAME = "activity";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_ID_ITEM = "iditem";
	public static final String COLUMN_ACTIVITY = "activity";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_PERSON = "person";
	public static final String[] AVAILABLE_COLUMNS = { COLUMN_ID,
			COLUMN_ID_ITEM, COLUMN_ACTIVITY, COLUMN_NAME, COLUMN_PERSON };

	// Database creation SQLite statement
	public static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME + "(" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_ACTIVITY
			+ " text," + COLUMN_NAME + " text," + COLUMN_PERSON + " text,"
			+ COLUMN_ID_ITEM + " integer);";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(Constants.TAG, "Upgrading database from version " + oldVersion
				+ " to " + newVersion + ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
	}

}

package es.pulimento.oye.summary;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.mcavallo.opencloud.Cloud;
import org.mcavallo.opencloud.Tag;

import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import es.pulimento.oye.R;
import es.pulimento.oye.dao.DAOCallLog;
import es.pulimento.oye.misc.Constants;
import es.pulimento.oye.misc.customview.TextViewRobotoSlab;
import es.pulimento.oye.misc.tag.Tag3DView;
import es.pulimento.oye.misc.tag.Tag3DView.TagBundle;
import es.pulimento.oye.misc.tag.TagCloud3DView;

public class SummaryCallFragment1 extends Fragment {

	private TagCloud3DView mTagCloudView;
	private ProgressBar mIndeterminateProgressBar;
	private FrameLayout rootLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_cloud_pre, container, false);

		rootLayout = (FrameLayout) rootView.findViewById(R.id.fl_cloud_pre);

		mIndeterminateProgressBar = (ProgressBar) rootView
				.findViewById(R.id.pb_cloud_pre);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		new CallsCloudGeneratorTask().execute();
	}

	private class CallsCloudGeneratorTask extends AsyncTask<Void, Void, List<Tag3DView>> {

		int width;
		int height;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			Log.w(Constants.TAG, "Tab 1 PRE ====================");

			Display display = getActivity().getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			width = size.x;
			height = size.y; // Approx, action bar and tab controller
		}

		@Override
		protected List<Tag3DView> doInBackground(Void... params) {
			DAOCallLog d = new DAOCallLog();
			Future<List<String>> callers = d.getAllCallers(getActivity());
			List<Tag3DView> myTagList = null;
			try {
				myTagList = generate3DCloud(generateCloud(callers.get()));
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			return myTagList;
		}

		@Override
		protected void onPostExecute(List<Tag3DView> result) {
			super.onPostExecute(result);
			Log.w(Constants.TAG, "Tab 1 POS ====================");

			TextView tv = new TextViewRobotoSlab(getActivity());
			tv.setTextSize(18);
			FrameLayout.LayoutParams tvParams = new FrameLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.TOP);
			tvParams.setMargins(12, 12, 12, 0); // ltrb

			if (result == null) {
				tv.setText(getString(R.string.tab_summary_empty_call_callers_cloud));
				rootLayout.addView(tv, tvParams);
				return;
			}

			// Create the 3D Tag Cloud view
			mTagCloudView = new TagCloud3DView(getActivity(), width, height, result);

			// setup views
			rootLayout
					.setBackgroundColor(getResources().getColor(R.color.background_app));
			mTagCloudView.requestFocus();
			mTagCloudView.setFocusableInTouchMode(true);

			// setup header textview
			tv.setText(getString(R.string.tab_summary_call_callers_cloud_text));

			// add views to the fragment layout
			rootLayout.addView(mTagCloudView);
			rootLayout.addView(tv, tvParams);

			// hide indeterminate progress indicator
			mIndeterminateProgressBar.setVisibility(View.GONE);
		}

		private Tag3DView createTag(final String text, final int popularity,
				final int color) {
			final TagBundle bundle = new TagBundle(text, popularity, color);
			return new Tag3DView(getActivity(), bundle);
		}

		Comparator<Tag> comparator = new Comparator<Tag>() {

			public int compare(Tag lhs, Tag rhs) {
				return lhs.getScoreInt() + rhs.getScoreInt();
			};
		};

		public List<Tag> generateCloud(List<String> callers) {
			Cloud c = new Cloud();
			c.setMaxTagsToDisplay(30);
			c.setMaxWeight(32);
			c.setMinWeight(28);

			for (String s : callers) {
				c.addTag(s);
			}
			return c.tags(comparator);
		}

		public List<Tag3DView> generate3DCloud(List<Tag> cloud) {
			if (cloud.isEmpty())
				return null;

			List<Tag3DView> res = new ArrayList<Tag3DView>();
			int i = 0;
			int MAX_RESULTS = Math.min(cloud.size(), 12);

			Tag tag = null;

			for (; i < MAX_RESULTS; i++) {
				tag = cloud.get(i);
				res.add(createTag(tag.getName(), tag.getScoreInt(),
						R.color.cloud_individual_tag));
			}
			return res;
		}
	}

}

package es.pulimento.oye;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Intent;
import android.location.Location;
import android.util.Log;

import com.google.android.gms.location.LocationClient;

import es.pulimento.oye.dao.DAOPosition;
import es.pulimento.oye.db.PositionTable;

public class LocationService extends IntentService {

	private String TAG = this.getClass().getSimpleName();

	public LocationService() {
		super("Fused Location");
	}

	public LocationService(String name) {
		super("Fused Location");
	}

	@Override
	protected void onHandleIntent(Intent intent) {

		Location location = intent
				.getParcelableExtra(LocationClient.KEY_LOCATION_CHANGED);
		if (location != null) {
			Log.i(TAG, "onHandleIntent " + location.getLatitude() + ","
					+ location.getLongitude());

			ContentValues cv = new ContentValues();
			cv.put(PositionTable.COLUMN_ACCURACY, location.getAccuracy());
			cv.put(PositionTable.COLUMN_LATITUDE, location.getLatitude());
			cv.put(PositionTable.COLUMN_LONGITUDE, location.getLongitude());
			DAOPosition d = new DAOPosition();
			d.insert(LocationService.this, cv);
		}
	}

}
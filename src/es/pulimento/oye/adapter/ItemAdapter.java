package es.pulimento.oye.adapter;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fortysevendeg.android.swipelistview.SwipeListView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import es.pulimento.oye.R;
import es.pulimento.oye.bean.CallLogItem;
import es.pulimento.oye.bean.CommentItem;
import es.pulimento.oye.bean.GeneratedActivity;
import es.pulimento.oye.bean.ItemEvent;
import es.pulimento.oye.bean.PositionEvent;
import es.pulimento.oye.bean.SongPlayed;
import es.pulimento.oye.bean.TwitterItem;
import es.pulimento.oye.dao.DAOItem;
import es.pulimento.oye.misc.Constants;

public class ItemAdapter extends BaseAdapter {

	private List<ItemEvent> data;
	private Context context;

	public ItemAdapter(Context context, List<ItemEvent> data) {
		this.context = context;
		this.data = data;
	}

	@Override
	public int getCount() {
		return data.size();
	}

	@Override
	public ItemEvent getItem(int position) {
		return data.get(position);
	}

	@Override
	public long getItemId(int position) {
		return data.get(position).getIdItem();
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ItemEvent item = getItem(position);
		ViewHolder holder;
		int viewType = getItemViewType(position);
		if (convertView == null) {
			LayoutInflater li = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			holder = new ViewHolder();
			switch (viewType) {
			case Constants.VIEW_HEADSET_PLUG:
				convertView = li.inflate(R.layout.row_headset, parent, false);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.row_headset_title);
				holder.tv1 = (TextView) convertView.findViewById(R.id.row_headset_tv1);
				holder.tv2 = (TextView) convertView.findViewById(R.id.row_headset_tv2);
				holder.tv3 = (TextView) convertView.findViewById(R.id.row_headset_tv3);
				convertView.setTag(holder);
				break;
			case Constants.VIEW_POSITION:
				convertView = li.inflate(R.layout.row_position, parent, false);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.row_position_title);
				holder.tv1 = (TextView) convertView.findViewById(R.id.row_position_tv1);
				holder.tv2 = (TextView) convertView.findViewById(R.id.row_position_tv2);
				holder.tv3 = (TextView) convertView.findViewById(R.id.row_position_tv3);
				convertView.setTag(holder);
				break;
			case Constants.VIEW_CALL_LOG:
				convertView = li.inflate(R.layout.row_call_log, parent, false);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.row_call_log_title);
				holder.tv1 = (TextView) convertView.findViewById(R.id.row_call_log_tv1);
				holder.tv2 = (TextView) convertView.findViewById(R.id.row_call_log_tv2);
				holder.tv3 = (TextView) convertView.findViewById(R.id.row_call_log_tv3);
				convertView.setTag(holder);
				break;
			case Constants.VIEW_TWITTER:
				convertView = li.inflate(R.layout.row_twitter_item, parent, false);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.row_twitter_title);
				holder.tv1 = (TextView) convertView.findViewById(R.id.row_twitter_tv1);
				holder.tv2 = (TextView) convertView.findViewById(R.id.row_twitter_tv2);
				holder.tv3 = (TextView) convertView.findViewById(R.id.row_twitter_tv3);
				convertView.setTag(holder);
				break;
			case Constants.VIEW_COMMENT:
				convertView = li.inflate(R.layout.row_comment, parent, false);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.row_comment_title);
				holder.tv1 = (TextView) convertView.findViewById(R.id.row_comment_tv1);
				holder.tv2 = (TextView) convertView.findViewById(R.id.row_comment_tv2);
				holder.tv3 = (TextView) convertView.findViewById(R.id.row_comment_tv3);
				convertView.setTag(holder);
				break;
			case Constants.VIEW_ACTIVITY:
				convertView = li.inflate(R.layout.row_activity, parent, false);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.row_activity_title);
				holder.tv1 = (TextView) convertView.findViewById(R.id.row_activity_tv1);
				holder.tv2 = (TextView) convertView.findViewById(R.id.row_activity_tv2);
				holder.tv3 = (TextView) convertView.findViewById(R.id.row_activity_tv3);
				convertView.setTag(holder);
				break;
			case Constants.VIEW_SONG_PLAYED:
				convertView = li.inflate(R.layout.row_song_played, parent, false);
				holder.icon = (ImageView) convertView
						.findViewById(R.id.row_song_played_title);
				holder.tv1 = (TextView) convertView
						.findViewById(R.id.row_song_played_tv1);
				holder.tv2 = (TextView) convertView
						.findViewById(R.id.row_song_played_tv2);
				holder.tv3 = (TextView) convertView
						.findViewById(R.id.row_song_played_tv3);
				convertView.setTag(holder);
				break;
			default:
				Log.e(Constants.TAG,
						"Horror, getting a view without type in ItemAdapter!");
				break;
			}

			// holder.bAction1 = (Button) convertView
			// .findViewById(R.id.row_back_open);
			holder.shareButton = (ImageView) convertView
					.findViewById(R.id.row_back_share);
			holder.deleteButton = (ImageView) convertView
					.findViewById(R.id.row_back_delete);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		((SwipeListView) parent).recycle(convertView, position);

		switch (viewType) {
		case Constants.VIEW_HEADSET_PLUG:
			holder.icon.setImageResource(R.drawable.ic_row_call_answered);
			break;
		case Constants.VIEW_POSITION:
			holder.icon.setImageResource(R.drawable.ic_row_position);
			break;
		case Constants.VIEW_CALL_LOG:
			int callType = ((CallLogItem) item).getCallType();
			if (callType == CallLogItem.TYPE_INCOMING)
				holder.icon.setImageResource(R.drawable.ic_row_call_answered);
			else if (callType == CallLogItem.TYPE_OUTGOING)
				holder.icon.setImageResource(R.drawable.ic_row_call_outgoing);
			else if (callType == CallLogItem.TYPE_MISSED)
				holder.icon.setImageResource(R.drawable.ic_row_call_missed);
			break;
		case Constants.VIEW_TWITTER:
			int tweetType = ((TwitterItem) item).getTweetType();
			if (tweetType == TwitterItem.TYPE_USER_MENTION)
				holder.icon.setImageResource(R.drawable.ic_row_tw_mention);
			else if (tweetType == TwitterItem.TYPE_USER_RETWEET)
				holder.icon.setImageResource(R.drawable.ic_row_tw_rt);
			else if (tweetType == TwitterItem.TYPE_USER_TIMELINE)
				holder.icon.setImageResource(R.drawable.ic_row_tw_tweet);
			break;
		case Constants.VIEW_COMMENT:
			holder.icon.setImageResource(R.drawable.ic_row_comment);
			break;
		case Constants.VIEW_ACTIVITY:
			holder.icon.setImageResource(R.drawable.ic_row_activity);
			break;
		case Constants.VIEW_SONG_PLAYED:
			holder.icon.setImageResource(R.drawable.ic_row_song_played);
			break;
		default:
			Log.e(Constants.TAG, "Horror, getting a view without type in ItemAdapter!");
			break;
		}

		holder.tv1.setText(item.getField1() != null ? item.getField1() : "NULL :(");
		holder.tv2.setText(item.getField2() != null ? item.getField2() : "NULL :(");
		holder.tv3.setText(String.valueOf(DateUtils.getRelativeTimeSpanString(item
				.getTimestamp())));

		// holder.bAction1.setOnClickListener(new View.OnClickListener() {
		// @Override
		// public void onClick(View v) {
		// Toast.makeText(context, "OPEN!!", Toast.LENGTH_SHORT).show();
		// }
		// });

		holder.shareButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				share(item);
			}
		});

		holder.deleteButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				DAOItem d = new DAOItem();
				d.deleteItem(context, item.getIdItem(), item.getType());
				Toast.makeText(context, "DELETE!!", Toast.LENGTH_SHORT).show();
			}
		});

		return convertView;
	}

	static class ViewHolder {
		ImageView icon;
		TextView tv1;
		TextView tv2;
		TextView tv3;
		ImageView shareButton;
		ImageView deleteButton;
	}

	@Override
	public int getItemViewType(int position) {
		final int type = getItem(position).getType();
		switch (type) {
		case Constants.TYPE_ACTIVITY:
			return Constants.VIEW_ACTIVITY;
		case Constants.TYPE_CALL_LOG:
			return Constants.VIEW_CALL_LOG;
		case Constants.TYPE_HEADSET_PLUG:
			return Constants.VIEW_HEADSET_PLUG;
		case Constants.TYPE_POSITION:
			return Constants.VIEW_POSITION;
		case Constants.TYPE_TWITTER:
			return Constants.VIEW_TWITTER;
		case Constants.TYPE_COMMENT:
			return Constants.VIEW_COMMENT;
		case Constants.TYPE_SONG_PLAYED:
			return Constants.VIEW_SONG_PLAYED;
		default:
			return -1;
		}

	}

	// ====================== Utility methods =======================

	@Override
	public int getViewTypeCount() {
		return 7;
	}

	public long getFirstTimestamp() {
		return data.get(0).getTimestamp();
	}

	public long getLastTimestamp() {
		return data.get(data.size() - 1).getTimestamp();
	}

	public int getOffset() {
		return data.size();
	}

	public void deleteMultipleItems(Context context, Collection<Integer> itemsToDelete) {
		DAOItem d = new DAOItem();
		Collection<Integer> ids = new HashSet<Integer>();
		for (Integer i : itemsToDelete)
			ids.add(getItem(i).getIdItem());
		d.deleteItem(context, ids);
	}

	void share(ItemEvent item) {
		if (item == null) {
			Crouton.makeText((Activity) context, "Error al compartir", Style.ALERT)
					.show();
			return;
		}
		Intent sendIntent = new Intent();
		sendIntent.setAction(Intent.ACTION_SEND);
		String toShare = "";
		switch (item.getType()) {
		case Constants.TYPE_ACTIVITY:
			GeneratedActivity g = ((GeneratedActivity) item);
			toShare = "Estuve " + g.getActivity() + " con " + g.getPerson() + " "
					+ Constants.SHARE_TWITTER_HASHTAG;
			break;
		case Constants.TYPE_CALL_LOG:
			toShare = "Habl� con " + ((CallLogItem) item).getName() + " "
					+ Constants.SHARE_TWITTER_HASHTAG;
			break;
		case Constants.TYPE_HEADSET_PLUG:
			toShare = "�De verdad piensas compartir esto? :P";
			break;
		case Constants.TYPE_POSITION:
			PositionEvent p = ((PositionEvent) item);
			String url = "http://maps.google.com/maps?z=12&q=loc:" + p.getLatitude()
					+ "+" + p.getLongitude();
			toShare = "Estuve en " + p.getLatitude() + "," + p.getLongitude() + " - "
					+ url + " " + Constants.SHARE_TWITTER_HASHTAG;
			break;
		case Constants.TYPE_TWITTER:
			toShare = "Mira lo que tuite�: " + ((TwitterItem) item).getText() + " "
					+ Constants.SHARE_TWITTER_HASHTAG;
			break;
		case Constants.TYPE_COMMENT:
			toShare = ((CommentItem) item).getText() + " "
					+ Constants.SHARE_TWITTER_HASHTAG;
			break;
		case Constants.TYPE_SONG_PLAYED:
			SongPlayed s = ((SongPlayed) item);
			toShare = "He escuchado " + s.getSongTitle() + ", de " + s.getSongArtist()
					+ " " + Constants.SHARE_TWITTER_HASHTAG;
			break;
		}
		sendIntent.putExtra(Intent.EXTRA_TEXT, toShare);
		sendIntent.setType("text/plain");
		context.startActivity(Intent.createChooser(sendIntent, "Comparte"));
	}
}

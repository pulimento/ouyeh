package es.pulimento.oye.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import es.pulimento.oye.misc.BaseReceiver;

public class PackageManagerReceiver extends BaseReceiver {

	private static final String OWN_NAME = "Package Manager";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();

		if (null == bundle)
			return;

		potter(context, intent, OWN_NAME, true, null);
	}
}
package es.pulimento.oye.misc.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewOpen extends TextView {

	public TextViewOpen(Context context) {
		super(context);
		useCustomTypography();
	}

	public TextViewOpen(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		useCustomTypography();
	}

	public TextViewOpen(Context context, AttributeSet attrs) {
		super(context, attrs);
		useCustomTypography();
	}

	private void useCustomTypography() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"fonts/OpenSans-Regular.ttf");
		setTypeface(tf, 1);
	}
}

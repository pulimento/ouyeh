package es.pulimento.oye.misc;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import es.pulimento.oye.R;

public class MC extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Se mira pero no se toca
		FrameLayout l = new FrameLayout(this);
		l.setLayoutParams(new LayoutParams(403, 750));
		l.setBackgroundResource(R.drawable.img_calendar_picker);
		l.setClickable(true);
		l.setLongClickable(false);
		l.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
		
		setContentView(l);
	}
}

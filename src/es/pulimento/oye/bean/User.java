package es.pulimento.oye.bean;

public class User {

	private String _id;
	private String google_id;
	private String gg_given_name;
	private String gg_family_name;
	private String gg_picture;
	private int gg_gender;
	private String gg_locale;
	private int tw_id;
	private String tw_picture;
	private int tw_since;
	private String tw_description;
	private int tw_followers;
	private int tw_friends;
	private String tw_locale;
	private String tw_location;
	private String tw_name;
	private String tw_screen_name;
	private int tw_number_of_tweets;

	public User(String _id, String google_id, String given_name,
			String family_name, String picture, int gender, String locale) {
		super();
		this._id = _id;
		this.google_id = google_id;
		this.gg_given_name = given_name;
		this.gg_family_name = family_name;
		this.gg_picture = picture;
		this.gg_gender = gender;
		this.gg_locale = locale;
	}

	public User() {
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

	public String getGoogle_id() {
		return google_id;
	}

	public void setGoogle_id(String google_id) {
		this.google_id = google_id;
	}

	public String getGiven_name() {
		return gg_given_name;
	}

	public void setGiven_name(String given_name) {
		this.gg_given_name = given_name;
	}

	public String getFamily_name() {
		return gg_family_name;
	}

	public void setFamily_name(String family_name) {
		this.gg_family_name = family_name;
	}

	public String getPicture() {
		return gg_picture;
	}

	public void setPicture(String picture) {
		this.gg_picture = picture;
	}

	public int getGender() {
		return gg_gender;
	}

	public void setGender(int gender) {
		this.gg_gender = gender;
	}

	public String getLocale() {
		return gg_locale;
	}

	public void setLocale(String locale) {
		this.gg_locale = locale;
	}

	public int getTw_id() {
		return tw_id;
	}

	public void setTw_id(int tw_id) {
		this.tw_id = tw_id;
	}

	public String getTw_picture() {
		return tw_picture;
	}

	public void setTw_picture(String tw_picture) {
		this.tw_picture = tw_picture;
	}

	public int getTw_since() {
		return tw_since;
	}

	public void setTw_since(int tw_since) {
		this.tw_since = tw_since;
	}

	public String getTw_description() {
		return tw_description;
	}

	public void setTw_description(String tw_description) {
		this.tw_description = tw_description;
	}

	public int getTw_followers() {
		return tw_followers;
	}

	public void setTw_followers(int tw_followers) {
		this.tw_followers = tw_followers;
	}

	public int getTw_friends() {
		return tw_friends;
	}

	public void setTw_friends(int tw_friends) {
		this.tw_friends = tw_friends;
	}

	public String getTw_locale() {
		return tw_locale;
	}

	public void setTw_locale(String tw_locale) {
		this.tw_locale = tw_locale;
	}

	public String getTw_location() {
		return tw_location;
	}

	public void setTw_location(String tw_location) {
		this.tw_location = tw_location;
	}

	public String getTw_name() {
		return tw_name;
	}

	public void setTw_name(String tw_name) {
		this.tw_name = tw_name;
	}

	public String getTw_screen_name() {
		return tw_screen_name;
	}

	public void setTw_screen_name(String tw_screen_name) {
		this.tw_screen_name = tw_screen_name;
	}

	public int getTw_number_of_tweets() {
		return tw_number_of_tweets;
	}

	public void setTw_number_of_tweets(int tw_number_of_tweets) {
		this.tw_number_of_tweets = tw_number_of_tweets;
	}

	@Override
	public String toString() {
		return "User [_id=" + _id + ", google_id=" + google_id
				+ ", given_name=" + gg_given_name + ", family_name="
				+ gg_family_name + ", picture=" + gg_picture + ", gender="
				+ gg_gender + ", locale=" + gg_locale + "]";
	}

}

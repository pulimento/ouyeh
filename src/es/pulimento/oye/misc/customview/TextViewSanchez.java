package es.pulimento.oye.misc.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewSanchez extends TextView {

	public TextViewSanchez(Context context) {
		super(context);
		useCustomTypography();
	}

	public TextViewSanchez(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		useCustomTypography();
	}

	public TextViewSanchez(Context context, AttributeSet attrs) {
		super(context, attrs);
		useCustomTypography();
	}

	private void useCustomTypography() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"fonts/Sanchez-Regular.ttf");
		setTypeface(tf, 1);
	}
}

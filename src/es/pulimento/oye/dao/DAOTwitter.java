package es.pulimento.oye.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import twitter4j.ResponseList;
import twitter4j.Status;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;
import android.util.Log;
import es.pulimento.oye.bean.TwitterItem;
import es.pulimento.oye.db.CallLogTable;
import es.pulimento.oye.db.ItemTable;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.db.TwitterTable;
import es.pulimento.oye.misc.Constants;

public class DAOTwitter {

	String username;

	public DAOTwitter() {

	}

	public Future<Integer> insert(final Context context,
			final ResponseList<Status> statuses) {

		username = PreferenceManager.getDefaultSharedPreferences(context).getString(
				Constants.PREFS_TWITTER_USERNAME, "");

		Callable<Integer> c = new Callable<Integer>() {

			@Override
			public Integer call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				Integer inserted = -1;

				for (Status s : statuses) {
					ContentValues cvItem = new ContentValues();
					// Magic happens here
					cvItem.put(ItemTable.COLUMN_TIMESTAMP, s.getCreatedAt().getTime());
					cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_TWITTER);

					long idItem = db.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
					ContentValues values = new ContentValues();
					values.put(CallLogTable.COLUMN_ID_ITEM, idItem);
					values.put(TwitterTable.COLUMN_FETCHED_AT, System.currentTimeMillis());
					values.put(
							TwitterTable.COLUMN_GEOLOCATION,
							s.getGeoLocation() != null ? String.valueOf(s
									.getGeoLocation().getLatitude())
									+ String.valueOf(s.getGeoLocation().getLongitude())
									: null);
					values.put(TwitterTable.COLUMN_PLACE, s.getPlace() != null ? s
							.getPlace().getFullName() : null);
					values.put(TwitterTable.COLUMN_RETWEET_COUNT, s.getRetweetCount());
					values.put(TwitterTable.COLUMN_SOURCE, s.getSource());
					values.put(TwitterTable.COLUMN_TEXT, s.getText());
					values.put(TwitterTable.COLUMN_USER, s.getUser().getScreenName());
					values.put(TwitterTable.COLUMN_TWITTER_ID, s.getId());
					// TODO fetch more info from twitter users?

					if (username.equals(s.getUser().getScreenName())) {
						if (s.isRetweetedByMe()) {
							values.put(TwitterTable.COLUMN_TWEET_TYPE,
									TwitterItem.TYPE_USER_RETWEET);
						} else {
							values.put(TwitterTable.COLUMN_TWEET_TYPE,
									TwitterItem.TYPE_USER_TIMELINE);
						}
					} else {
						if (s.isRetweet()) {
							values.put(TwitterTable.COLUMN_TWEET_TYPE,
									TwitterItem.TYPE_USER_RETWEET);
						} else {
							values.put(TwitterTable.COLUMN_TWEET_TYPE,
									TwitterItem.TYPE_USER_MENTION);
						}
					}

					inserted = (int) db
							.insert(TwitterTable.TABLE_NAME, "<empty>", values);
				}

				// db.close();

				return inserted;
			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

	public Future<Long> getNewerTweet(final Context context) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				long newerTweetId = -1;

				Cursor c = db.query(TwitterTable.TABLE_NAME,
						new String[] { TwitterTable.COLUMN_TWITTER_ID }, null, null,
						null, null, TwitterTable.COLUMN_TWITTER_ID + " DESC", "1");

				if (c.moveToFirst()) {
					newerTweetId = c.getLong(0);// Only one column
				}

				// db.close();

				return newerTweetId;
			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

	public void deleteAllTweetsUSEWITHCAUTION(boolean iConfirmThat, final Context context) {
		if (iConfirmThat) {
			Thread t = new Thread(new Runnable() {
				@Override
				public void run() {
					MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
					SQLiteDatabase db = dbHelper.getWritableDatabase();
					db.delete(TwitterTable.TABLE_NAME, null, null);
					Log.w(Constants.TAG, "Deleted all tweets");
				}
			});
			t.start();
		}
	}

	public Future<List<String>> getAllUserTweetsOffline(final Context context) {

		Callable<List<String>> c = new Callable<List<String>>() {

			@Override
			public List<String> call() throws Exception {

				username = PreferenceManager.getDefaultSharedPreferences(context)
						.getString(Constants.PREFS_TWITTER_USERNAME, "");

				if ("".equals(username)) {
					return null;
				}

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				ArrayList<String> items = new ArrayList<String>();

				SQLiteDatabase db = dbHelper.getWritableDatabase();

				Cursor cursor = db.query(TwitterTable.TABLE_NAME,
						new String[] { TwitterTable.COLUMN_TEXT },
						TwitterTable.COLUMN_USER + "=?", new String[] { username }, null,
						null, null);

				while (cursor.moveToNext()) {
					String tweetText = cursor.getString(cursor
							.getColumnIndexOrThrow(TwitterTable.COLUMN_TEXT));
					items.add(tweetText);
				}

				return items;

			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}
}

package es.pulimento.oye.fetcher;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.database.Cursor;
import android.os.AsyncTask;
import android.provider.CallLog;
import es.pulimento.oye.bean.CallLogItem;
import es.pulimento.oye.dao.DAOCallLog;

public class CallLogFetcher {

	private Context ctx;

	public CallLogFetcher(Context ctx) {
		this.ctx = ctx;

		// new ReadLogs().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		new ReadLogs().execute();
	}

	private String getDuration(long milliseconds) {
		int seconds = (int) (milliseconds / 1000) % 60;
		int minutes = (int) ((milliseconds / (1000 * 60)) % 60);
		int hours = (int) ((milliseconds / (1000 * 60 * 60)) % 24);
		if (hours < 1)
			return minutes + ":" + seconds;
		return hours + ":" + minutes + ":" + seconds;
	}

	private List<CallLogItem> readCallLogs() {

		DAOCallLog d = new DAOCallLog();

		Long newerTimestamp = -1L;
		try {
			newerTimestamp = d.getNewerTimestamp(ctx).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		String selection = newerTimestamp != 1L ? CallLog.Calls.DATE + " > "
				+ String.valueOf(newerTimestamp) : null;

		String[] projection = new String[] { CallLog.Calls.TYPE, CallLog.Calls.DURATION,
				CallLog.Calls.CACHED_NAME, CallLog.Calls.CACHED_NUMBER_LABEL,
				CallLog.Calls.NUMBER, CallLog.Calls.DATE };

		Cursor callLogCursor = ctx.getContentResolver().query(
				android.provider.CallLog.Calls.CONTENT_URI, projection, selection, null,
				android.provider.CallLog.Calls.DATE + " DESC");
		if (callLogCursor != null) {
			List<CallLogItem> callLogs = new ArrayList<CallLogItem>();
			while (callLogCursor.moveToNext()) {
				String name = callLogCursor.getString(callLogCursor
						.getColumnIndex(CallLog.Calls.CACHED_NAME));
				String cacheNumber = callLogCursor.getString(callLogCursor
						.getColumnIndex(CallLog.Calls.CACHED_NUMBER_LABEL));
				String number = callLogCursor.getString(callLogCursor
						.getColumnIndex(CallLog.Calls.NUMBER));
				long dateTimeMillis = callLogCursor.getLong(callLogCursor
						.getColumnIndex(CallLog.Calls.DATE));
				long durationMillis = callLogCursor.getLong(callLogCursor
						.getColumnIndex(CallLog.Calls.DURATION));
				int callType = callLogCursor.getInt(callLogCursor
						.getColumnIndex(CallLog.Calls.TYPE));

				String duration = durationMillis != 0L ? getDuration(durationMillis * 1000)
						: null;

				if (cacheNumber == null)
					cacheNumber = number;
				if (name == null)
					name = "<Sin Nombre>";

				CallLogItem call = new CallLogItem();

				call.setName(name);
				call.setNumber(number);
				call.setCallTimestamp(dateTimeMillis);
				call.setDurationString(duration);
				call.setCallType(callType);

				// d.insert(ctx, call);

				callLogs.add(call);
			}
			callLogCursor.close();

			d.insert(ctx, callLogs);

			return callLogs;
		}

		return null;
	}

	private class ReadLogs extends AsyncTask<Void, Void, Void> {

		@Override
		protected Void doInBackground(Void... params) {
			readCallLogs();
			return null;
		}
	}
}

package es.pulimento.oye.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import es.pulimento.oye.bean.CallLogItem;
import es.pulimento.oye.db.CallLogTable;
import es.pulimento.oye.db.ItemTable;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.misc.Constants;

public class DAOCallLog {

	public DAOCallLog() {
	}

	// public Future<Long> insert(final Context context, final ContentValues
	// values) {
	//
	// Callable<Long> c = new Callable<Long>() {
	//
	// @Override
	// public Long call() throws Exception {
	// long id = -1;
	//
	// ContentValues cvItem = new ContentValues();
	// cvItem.put(ItemTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
	// cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_CALL_LOG);
	//
	// MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
	// SQLiteDatabase db = dbHelper.getWritableDatabase();
	//
	// long idItem = db.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
	// values.put(HeadsetPlugTable.COLUMN_ID_ITEM, idItem);
	// id = db.insert(HeadsetPlugTable.TABLE_NAME, "<empty>", values);
	//
	// db.close();
	//
	// return id;
	// };
	// };
	// ScheduledExecutorService s =
	// Executors.newSingleThreadScheduledExecutor();
	// return s.submit(c);
	// }
	//
	// public Future<Long> insert(final Context context, final CallLogItem call)
	// {
	//
	// Callable<Long> c = new Callable<Long>() {
	//
	// @Override
	// public Long call() throws Exception {
	// long id = -1;
	//
	// ContentValues cvItem = new ContentValues();
	// cvItem.put(ItemTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
	// cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_CALL_LOG);
	//
	// MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
	// SQLiteDatabase db = dbHelper.getWritableDatabase();
	//
	// long idItem = db.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
	// ContentValues values = new ContentValues();
	// values.put(CallLogTable.COLUMN_ID_ITEM, idItem);
	// values.put(CallLogTable.COLUMN_CALL_TIMESTAMP, call.getCallTimestamp());
	// values.put(CallLogTable.COLUMN_CALL_TYPE, call.getCallType());
	// values.put(CallLogTable.COLUMN_DURATION, call.getDurationString());
	// values.put(CallLogTable.COLUMN_NAME, call.getName());
	// values.put(CallLogTable.COLUMN_NUMBER, call.getNumber());
	// id = db.insert(CallLogTable.TABLE_NAME, "<empty>", values);
	//
	// db.close();
	//
	// return id;
	// };
	// };
	// ScheduledExecutorService s =
	// Executors.newSingleThreadScheduledExecutor();
	// return s.submit(c);
	// }

	public Future<Long> insert(final Context context, final List<CallLogItem> calls) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				long lastIdInserted = -1;

				for (CallLogItem c : calls) {
					ContentValues cvItem = new ContentValues();
					cvItem.put(ItemTable.COLUMN_TIMESTAMP, c.getCallTimestamp());
					cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_CALL_LOG);

					long idItem = db.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
					ContentValues values = new ContentValues();
					values.put(CallLogTable.COLUMN_ID_ITEM, idItem);
					values.put(CallLogTable.COLUMN_FETCHED_AT, System.currentTimeMillis());
					values.put(CallLogTable.COLUMN_CALL_TYPE, c.getCallType());
					values.put(CallLogTable.COLUMN_DURATION, c.getDurationString());
					values.put(CallLogTable.COLUMN_NAME, c.getName());
					values.put(CallLogTable.COLUMN_NUMBER, c.getNumber());
					lastIdInserted = db
							.insert(CallLogTable.TABLE_NAME, "<empty>", values);
				}

				// db.close();

				return lastIdInserted;
			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

	public Future<Long> getNewerTimestamp(final Context context) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				long newerTimestamp = -1;

				Cursor c = db.query(ItemTable.TABLE_NAME,
						new String[] { ItemTable.COLUMN_TIMESTAMP }, "type=?",
						new String[] { String.valueOf(Constants.TYPE_CALL_LOG) }, null,
						null, ItemTable.COLUMN_TIMESTAMP + " DESC", "1");

				if (c.moveToFirst()) {
					newerTimestamp = c.getLong(0);// Only one column
				}

				// db.close();

				return newerTimestamp;
			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

	public Future<List<String>> getAllCallers(final Context context) {

		Callable<List<String>> c = new Callable<List<String>>() {

			@Override
			public List<String> call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper.getInstance(context);
				ArrayList<String> items = new ArrayList<String>();

				SQLiteDatabase db = dbHelper.getWritableDatabase();

				Cursor cursor = db.query(CallLogTable.TABLE_NAME,
						new String[] { CallLogTable.COLUMN_NAME }, null, null, null,
						null, null);

				while (cursor.moveToNext()) {
					String caller = cursor.getString(cursor
							.getColumnIndexOrThrow(CallLogTable.COLUMN_NAME));
					items.add(caller);
				}

				return items;

			};
		};
		ScheduledExecutorService s = Executors.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

}

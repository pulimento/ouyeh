package es.pulimento.oye.summary;

import java.util.Locale;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout;
import android.widget.TextView;
import es.pulimento.oye.R;
import es.pulimento.oye.misc.MF;
import es.pulimento.oye.misc.NonSwipeableViewPager;
import es.pulimento.oye.misc.customview.TextViewSanchez;

public class SummaryPositionActivity extends FragmentActivity implements
		ActionBar.TabListener {

	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter mSectionsPagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	NonSwipeableViewPager mViewPager;

	private TextView abTitle;
	private ImageView abLeft;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		setContentView(R.layout.activity_summary_twitter);

		final ActionBar actionBar = getActionBar();

		loadCustomActionBarWithTabs(actionBar);

		super.onCreate(savedInstanceState);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (NonSwipeableViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);

		// When swiping between different sections, select the corresponding
		// tab. We can also use ActionBar.Tab#select() to do this if we have
		// a reference to the Tab.
		mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}
		});

		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(actionBar.newTab()
					.setText(mSectionsPagerAdapter.getPageTitle(i)).setTabListener(this));
		}
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			Fragment fragment = null;
			switch (position) {
			case 0:
				fragment = new SummaryPositionFragment1();
				// fragment = new MF();
				break;
			case 1:
				// fragment = new SummaryTwitterFragment2();
				fragment = new MF();
				break;
			}
			return fragment;
		}

		@Override
		public int getCount() {
			// Number of tabs
			return 2;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.tab_summary_twitter_own_cloud).toUpperCase(l);
			case 1:
				return getString(R.string.tab_summary_twitter_others_cloud)
						.toUpperCase(l);
			}
			return null;
		}
	}

	void loadCustomActionBarWithTabs(final ActionBar actionBar) {
		// Define custom action bar...
		RelativeLayout ll = new RelativeLayout(this);

		abTitle = new TextViewSanchez(this);
		abTitle.setText(getString(R.string.title_ab_summary_position));
		abTitle.setTextColor(getResources().getColor(R.color.tv_ab_title));
		abTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 30);

		abLeft = new ImageView(this);
		abLeft.setImageResource(R.drawable.ic_ab_back);
		abLeft.setClickable(true);
		abLeft.setScaleType(ScaleType.FIT_CENTER);
		abLeft.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});

		RelativeLayout.LayoutParams pL = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		pL.addRule(RelativeLayout.ALIGN_PARENT_LEFT, abLeft.getId());
		ll.addView(abLeft, pL);

		View v = new View(this);
		RelativeLayout.LayoutParams pR = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		pR.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, v.getId());
		ll.addView(v, pR);

		RelativeLayout.LayoutParams pC = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		pC.addRule(RelativeLayout.CENTER_IN_PARENT, abTitle.getId());
		ll.addView(abTitle, pC);

		ll.setBackgroundColor(getResources().getColor(R.color.ab_background));

		actionBar.setCustomView(ll);
		actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);

		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Fix to avoid tab selector display above action bar
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);
		View homeIcon = findViewById(android.R.id.home);
		((View) homeIcon.getParent()).setVisibility(View.GONE);
	}

}

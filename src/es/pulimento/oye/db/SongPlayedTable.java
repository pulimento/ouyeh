package es.pulimento.oye.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import es.pulimento.oye.misc.Constants;

public class SongPlayedTable {

	// Database table
	public static final String TABLE_NAME = "songplayed";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_ID_ITEM = "iditem";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_ARTIST = "artist";
	public static final String COLUMN_MISC = "misc";
	public static final String COLUMN_ALBUM = "album";
	public static final String COLUMN_COVER_URL = "coverurl";

	// Database creation SQLite statement
	public static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME + "(" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_TITLE + " text, "
			+ COLUMN_MISC + " text," + COLUMN_ARTIST + " text," + COLUMN_ALBUM
			+ " text," + COLUMN_COVER_URL + " text, " + COLUMN_ID_ITEM
			+ " integer);";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(Constants.TAG, "Upgrading database from version " + oldVersion
				+ " to " + newVersion + ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
	}

}

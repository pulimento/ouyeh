package es.pulimento.oye.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.preference.PreferenceManager;
import android.util.Log;
import es.pulimento.oye.bean.CallLogItem;
import es.pulimento.oye.bean.CommentItem;
import es.pulimento.oye.bean.GeneratedActivity;
import es.pulimento.oye.bean.HeadsetEvent;
import es.pulimento.oye.bean.ItemEvent;
import es.pulimento.oye.bean.PositionEvent;
import es.pulimento.oye.bean.SongPlayed;
import es.pulimento.oye.bean.TwitterItem;
import es.pulimento.oye.db.ActivityTable;
import es.pulimento.oye.db.CallLogTable;
import es.pulimento.oye.db.CommentTable;
import es.pulimento.oye.db.HeadsetPlugTable;
import es.pulimento.oye.db.ItemTable;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.db.PositionTable;
import es.pulimento.oye.db.SongPlayedTable;
import es.pulimento.oye.db.TwitterTable;
import es.pulimento.oye.misc.Constants;

public class DAOItem {

	public final int MAX_RESULTS = 30;
	public final int MIN_RESULTS = 10;

	// MAGIC OCCURS HERE!!!

	public DAOItem() {
	}

	// If passing only the context, it return the N first items
	public Future<List<ItemEvent>> getItems(final Context context,
			final Boolean newItems, final long timestampToWhere) {

		final boolean showPosition = PreferenceManager
				.getDefaultSharedPreferences(context).getBoolean(
						Constants.PREFS_KEY_SHOW_POSITION, true);

		Callable<List<ItemEvent>> c = new Callable<List<ItemEvent>>() {

			@Override
			public List<ItemEvent> call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper
						.getInstance(context);
				ArrayList<ItemEvent> items = new ArrayList<ItemEvent>();

				// get items...
				SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();

				// Set the table
				queryBuilder.setTables(ItemTable.TABLE_NAME);

				SQLiteDatabase db = dbHelper.getWritableDatabase();

				Cursor cursor = null;

				String additionalWhere = "";
				String additionalWhereNP = "";

				if (!showPosition) {
					additionalWhere = " AND " + ItemTable.COLUMN_TYPE + "!="
							+ Constants.TYPE_POSITION;
					additionalWhereNP = ItemTable.COLUMN_TYPE + "!="
							+ Constants.TYPE_POSITION;
				}

				if (newItems != null) {
					if (newItems) {
						// Load newer items
						cursor = queryBuilder.query(db, null,
								ItemTable.COLUMN_TIMESTAMP + " > "
										+ timestampToWhere + additionalWhere,
								null, null, null, ItemTable.COLUMN_TIMESTAMP
										+ " DESC", String.valueOf(MAX_RESULTS));
					} else {
						// Load older items
						cursor = queryBuilder.query(db, null,
								ItemTable.COLUMN_TIMESTAMP + " < "
										+ timestampToWhere + additionalWhere,
								null, null, null, ItemTable.COLUMN_TIMESTAMP
										+ " DESC", String.valueOf(MAX_RESULTS));
					}
				} else {
					// No parameters, load N first items
					cursor = queryBuilder.query(db, null, additionalWhereNP,
							null, null, null, ItemTable.COLUMN_TIMESTAMP
									+ " DESC", String.valueOf(MAX_RESULTS));
				}

				while (cursor.moveToNext()) {
					int id = cursor.getInt(cursor
							.getColumnIndexOrThrow(ItemTable.COLUMN_ID));
					int type = cursor.getInt(cursor
							.getColumnIndexOrThrow(ItemTable.COLUMN_TYPE));
					long timestamp = cursor.getLong(cursor
							.getColumnIndexOrThrow(ItemTable.COLUMN_TIMESTAMP));
					switch (type) {
					case Constants.TYPE_HEADSET_PLUG:
						Log.i(Constants.TAG, "ItemDAO processing headset");
						Cursor hc = db.query(HeadsetPlugTable.TABLE_NAME, null,
								"iditem = " + id, null, null, null, null);
						if (hc.moveToFirst()) {
							HeadsetEvent h = new HeadsetEvent();
							h.setName(hc.getString(hc
									.getColumnIndexOrThrow(HeadsetPlugTable.COLUMN_NAME)));
							h.setMicrophone(hc.getInt(hc
									.getColumnIndexOrThrow(HeadsetPlugTable.COLUMN_MICROPHONE)));
							h.setState(hc.getInt(hc
									.getColumnIndexOrThrow(HeadsetPlugTable.COLUMN_STATE)));
							h.setTimestamp(timestamp);
							h.setIdItem(id);
							items.add(h);
							break;
						} else {
							Log.e(Constants.TAG,
									"no element returned by cursor");
						}
					case Constants.TYPE_POSITION:
						Log.i(Constants.TAG, "ItemDAO processing position");
						Cursor pc = db.query(PositionTable.TABLE_NAME, null,
								"iditem = " + id, null, null, null, null);
						if (pc.moveToFirst()) {
							PositionEvent p = new PositionEvent();
							p.setLatitude(pc.getDouble(pc
									.getColumnIndexOrThrow(PositionTable.COLUMN_LATITUDE)));
							p.setLongitude(pc.getDouble(pc
									.getColumnIndexOrThrow(PositionTable.COLUMN_LONGITUDE)));
							p.setAccuracy(pc.getFloat(pc
									.getColumnIndexOrThrow(PositionTable.COLUMN_ACCURACY)));
							p.setTimestamp(timestamp);
							p.setIdItem(id);
							items.add(p);
						} else {
							Log.e(Constants.TAG,
									"no element returned by cursor");
						}
						break;
					case Constants.TYPE_CALL_LOG:
						Log.i(Constants.TAG, "ItemDAO processing call log");
						Cursor cc = db.query(CallLogTable.TABLE_NAME, null,
								CallLogTable.TABLE_NAME + ".iditem = " + id,
								null, null, null, null);
						if (cc.moveToFirst()) {
							CallLogItem c = new CallLogItem();
							c.setCallType(cc.getInt(cc
									.getColumnIndexOrThrow(CallLogTable.COLUMN_CALL_TYPE)));
							c.setDurationString(cc.getString(cc
									.getColumnIndexOrThrow(CallLogTable.COLUMN_DURATION)));
							c.setName(cc.getString(cc
									.getColumnIndexOrThrow(CallLogTable.COLUMN_NAME)));
							c.setNumber(cc.getString(cc
									.getColumnIndexOrThrow(CallLogTable.COLUMN_NUMBER)));
							c.setTimestamp(timestamp);
							c.setIdItem(id);
							items.add(c);
						} else {
							Log.e(Constants.TAG,
									"no element returned by cursor");
						}
						break;
					case Constants.TYPE_TWITTER:
						Log.i(Constants.TAG, "ItemDAO processing tweet");
						Cursor ct = db.query(TwitterTable.TABLE_NAME, null,
								TwitterTable.TABLE_NAME + ".iditem = " + id,
								null, null, null, null);
						if (ct.moveToFirst()) {
							TwitterItem t = new TwitterItem();
							t.setFetchedAt(ct.getLong(ct
									.getColumnIndexOrThrow(TwitterTable.COLUMN_FETCHED_AT)));
							t.setGeolocation(ct.getString(ct
									.getColumnIndexOrThrow(TwitterTable.COLUMN_GEOLOCATION)));
							t.setPlace(ct.getString(ct
									.getColumnIndexOrThrow(TwitterTable.COLUMN_PLACE)));
							t.setRtCount(ct.getInt(ct
									.getColumnIndexOrThrow(TwitterTable.COLUMN_RETWEET_COUNT)));
							t.setSource(ct.getString(ct
									.getColumnIndexOrThrow(TwitterTable.COLUMN_SOURCE)));
							t.setText(ct.getString(ct
									.getColumnIndexOrThrow(TwitterTable.COLUMN_TEXT)));
							t.setUser(ct.getString(ct
									.getColumnIndexOrThrow(TwitterTable.COLUMN_USER)));
							t.setTweetType(ct.getInt(ct
									.getColumnIndexOrThrow(TwitterTable.COLUMN_TWEET_TYPE)));
							t.setTimestamp(timestamp);
							t.setIdItem(id);
							items.add(t);
						} else {
							Log.e(Constants.TAG,
									"no element returned by cursor");
						}
						break;
					case Constants.TYPE_COMMENT:
						Log.i(Constants.TAG, "ItemDAO processing comment");
						Cursor ccm = db.query(CommentTable.TABLE_NAME, null,
								CommentTable.TABLE_NAME + ".iditem = " + id,
								null, null, null, null);
						if (ccm.moveToFirst()) {
							CommentItem c = new CommentItem();
							c.setText(ccm.getString(ccm
									.getColumnIndexOrThrow(CommentTable.COLUMN_NAME)));
							c.setImageName(ccm.getString(ccm
									.getColumnIndexOrThrow(CommentTable.COLUMN_IMAGE_NAME)));
							c.setMisc(ccm.getString(ccm
									.getColumnIndexOrThrow(CommentTable.COLUMN_MISC)));
							c.setTimestamp(timestamp);
							c.setIdItem(id);
							items.add(c);
						} else {
							Log.e(Constants.TAG,
									"no element returned by cursor");
						}
						break;
					case Constants.TYPE_ACTIVITY:
						Log.i(Constants.TAG, "ItemDAO processing activity");
						Cursor cca = db.query(ActivityTable.TABLE_NAME, null,
								ActivityTable.TABLE_NAME + ".iditem = " + id,
								null, null, null, null);
						if (cca.moveToFirst()) {
							GeneratedActivity act = new GeneratedActivity();
							act.setActivity(cca.getString(cca
									.getColumnIndexOrThrow(ActivityTable.COLUMN_ACTIVITY)));
							act.setName(cca.getString(cca
									.getColumnIndexOrThrow(ActivityTable.COLUMN_NAME)));
							act.setPerson(cca.getString(cca
									.getColumnIndexOrThrow(ActivityTable.COLUMN_PERSON)));
							act.setTimestamp(timestamp);
							act.setIdItem(id);
							items.add(act);
						} else {
							Log.e(Constants.TAG,
									"no element returned by cursor");
						}
						break;
					case Constants.TYPE_SONG_PLAYED:
						Log.i(Constants.TAG, "ItemDAO processing song played");
						Cursor ccsp = db.query(SongPlayedTable.TABLE_NAME,
								null, SongPlayedTable.TABLE_NAME + ".iditem = "
										+ id, null, null, null, null);
						if (ccsp.moveToFirst()) {
							SongPlayed song = new SongPlayed();
							song.setSongTitle(ccsp.getString(ccsp
									.getColumnIndexOrThrow(SongPlayedTable.COLUMN_TITLE)));
							song.setSongAlbum(ccsp.getString(ccsp
									.getColumnIndexOrThrow(SongPlayedTable.COLUMN_ALBUM)));
							song.setSongArtist(ccsp.getString(ccsp
									.getColumnIndexOrThrow(SongPlayedTable.COLUMN_ARTIST)));
							song.setCoverURL(ccsp.getString(ccsp
									.getColumnIndexOrThrow(SongPlayedTable.COLUMN_COVER_URL)));
							song.setMisc(ccsp.getString(ccsp
									.getColumnIndexOrThrow(SongPlayedTable.COLUMN_MISC)));

							song.setTimestamp(timestamp);
							song.setIdItem(id);
							items.add(song);
						} else {
							Log.e(Constants.TAG,
									"no element returned by cursor");
						}
						break;
					default:
						Log.i(Constants.TAG, "ItemDAO processing UNKNOWN");
						break;
					}
					// db.close();
				}
				return items;
			};
		};
		ScheduledExecutorService s = Executors
				.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

	public void deleteItem(final Context context, final long idItemToDelete,
			final int type) {

		Log.d(Constants.TAG, "onDeleteItem single" + idItemToDelete);
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				long main = -1, particular = -1;

				MainDatabaseHelper dbHelper = MainDatabaseHelper
						.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				main = db.delete(ItemTable.TABLE_NAME, ItemTable.COLUMN_ID
						+ " = " + idItemToDelete, null);
				switch (type) {
				case Constants.TYPE_ACTIVITY:
					particular = db.delete(ActivityTable.TABLE_NAME,
							ActivityTable.COLUMN_ID_ITEM + " = "
									+ idItemToDelete, null);
					break;
				case Constants.TYPE_HEADSET_PLUG:
					particular = db.delete(HeadsetPlugTable.TABLE_NAME,
							HeadsetPlugTable.COLUMN_ID_ITEM + " = "
									+ idItemToDelete, null);
					break;
				case Constants.TYPE_CALL_LOG:
					particular = db.delete(CallLogTable.TABLE_NAME,
							CallLogTable.COLUMN_ID_ITEM + " = "
									+ idItemToDelete, null);
					break;
				case Constants.TYPE_COMMENT:
					particular = db.delete(CommentTable.TABLE_NAME,
							CommentTable.COLUMN_ID_ITEM + " = "
									+ idItemToDelete, null);
					break;
				case Constants.TYPE_SONG_PLAYED:
					particular = db.delete(SongPlayedTable.TABLE_NAME,
							SongPlayedTable.COLUMN_ID_ITEM + " = "
									+ idItemToDelete, null);
					break;
				case Constants.TYPE_TWITTER:
					particular = db.delete(TwitterTable.TABLE_NAME,
							TwitterTable.COLUMN_ID_ITEM + " = "
									+ idItemToDelete, null);
					break;
				}
				Log.i(Constants.TAG, "ITEM DELETED, id " + idItemToDelete
						+ ", general " + main + ", particular " + particular);
			}
		});
		t.start();
	}

	public void deleteItem(final Context context,
			final Collection<Integer> itemsToDelete) {
		Log.d(Constants.TAG, "onDeleteItem multiple" + itemsToDelete.size());
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {

				MainDatabaseHelper dbHelper = MainDatabaseHelper
						.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();

				for (Integer idItemToDelete : itemsToDelete) {
					long main = -1, particular = -1;
					int type = -1;

					Cursor guessType = db.query(ItemTable.TABLE_NAME,
							new String[] { ItemTable.COLUMN_TYPE },
							ItemTable.COLUMN_ID + " = " + idItemToDelete, null,
							null, null, null);
					if (guessType.moveToFirst()) {
						type = guessType.getInt(0);
					}
					main = db.delete(ItemTable.TABLE_NAME, ItemTable.COLUMN_ID
							+ " = " + idItemToDelete, null);
					switch (type) {
					case Constants.TYPE_ACTIVITY:
						particular = db.delete(ActivityTable.TABLE_NAME,
								ActivityTable.COLUMN_ID_ITEM + " = "
										+ idItemToDelete, null);
						break;
					case Constants.TYPE_HEADSET_PLUG:
						particular = db.delete(HeadsetPlugTable.TABLE_NAME,
								HeadsetPlugTable.COLUMN_ID_ITEM + " = "
										+ idItemToDelete, null);
						break;
					case Constants.TYPE_POSITION:
						particular = db.delete(PositionTable.TABLE_NAME,
								PositionTable.COLUMN_ID_ITEM + " = "
										+ idItemToDelete, null);
						break;
					case Constants.TYPE_COMMENT:
						particular = db.delete(CommentTable.TABLE_NAME,
								CommentTable.COLUMN_ID_ITEM + " = "
										+ idItemToDelete, null);
						break;
					case Constants.TYPE_SONG_PLAYED:
						particular = db.delete(SongPlayedTable.TABLE_NAME,
								SongPlayedTable.COLUMN_ID_ITEM + " = "
										+ idItemToDelete, null);
						break;
					case Constants.TYPE_TWITTER:
						particular = db.delete(TwitterTable.TABLE_NAME,
								TwitterTable.COLUMN_ID_ITEM + " = "
										+ idItemToDelete, null);
					default:
						Log.e(Constants.TAG,
								"Oh no!! Can't know the type, can't delete!");
					}
					Log.i(Constants.TAG, "ITEM DELETED, id " + idItemToDelete
							+ ", general " + main + ", particular "
							+ particular);
				}
			}
		});
		t.start();
	}
}

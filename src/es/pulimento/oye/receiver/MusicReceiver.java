package es.pulimento.oye.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import es.pulimento.oye.bean.SongPlayed;
import es.pulimento.oye.dao.DAOSongPlayed;
import es.pulimento.oye.misc.BaseReceiver;

public class MusicReceiver extends BaseReceiver {

	public static final String OWN_NAME = "MusicReceiver";

	@Override
	public void onReceive(Context context, Intent intent) {

		Bundle bundle = intent.getExtras();

		if (null == bundle) {
			return;
		}

		// HTC music
		Boolean isPlaying = intent.getBooleanExtra("isplaying", false);
		// Google Play Music
		boolean playing = intent.getBooleanExtra("playing", false);
		// Soundcloud
		boolean isSupposedToBePlaying = intent.getBooleanExtra(
				"isSupposedToBePlaying", false);
		String title = intent.getStringExtra("track");
		String artist = intent.getStringExtra("artist");
		String album = intent.getStringExtra("album");
		// String misc = String.valueOf(isPlaying);

		if (isSupposedToBePlaying) {
			artist = intent.getStringExtra("username");
			title = intent.getStringExtra("title");
		}

		if (isPlaying || playing || isSupposedToBePlaying) {
			SongPlayed s = new SongPlayed();
			s.setSongTitle(title);
			s.setSongAlbum(album);
			s.setSongArtist(artist);

			DAOSongPlayed d = new DAOSongPlayed();
			d.insert(context, s);
		} else {
			return;
		}

		potter(context, intent, OWN_NAME, true, null);
	}
}

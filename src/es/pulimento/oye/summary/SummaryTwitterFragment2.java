package es.pulimento.oye.summary;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.mcavallo.opencloud.Cloud;
import org.mcavallo.opencloud.Tag;
import org.mcavallo.opencloud.filters.MinLengthFilter;

import twitter4j.Paging;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import es.pulimento.oye.R;
import es.pulimento.oye.misc.Constants;
import es.pulimento.oye.misc.customview.TextViewRobotoSlab;
import es.pulimento.oye.misc.tag.Tag3DView;
import es.pulimento.oye.misc.tag.Tag3DView.TagBundle;
import es.pulimento.oye.misc.tag.TagCloud3DView;

public class SummaryTwitterFragment2 extends Fragment {

	private TagCloud3DView mTagCloudView;
	private ProgressBar mIndeterminateProgressBar;
	private FrameLayout rootLayout;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_cloud_pre, container, false);

		rootLayout = (FrameLayout) rootView.findViewById(R.id.fl_cloud_pre);

		mIndeterminateProgressBar = (ProgressBar) rootView
				.findViewById(R.id.pb_cloud_pre);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		new FetchFollowerTweets(getActivity()).execute();
	}

	private class FetchFollowerTweets extends AsyncTask<Void, Void, List<Tag3DView>> {

		SharedPreferences settings;
		Context context;
		Twitter twitter;
		int width;
		int height;

		FetchFollowerTweets(Context context) {
			this.context = context;
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			Log.w(Constants.TAG, "Tab 2 PRE ====================");

			Display display = getActivity().getWindowManager().getDefaultDisplay();
			Point size = new Point();
			display.getSize(size);
			width = size.x;
			height = size.y; // Approx, action bar and tab controller

			settings = PreferenceManager.getDefaultSharedPreferences(context);

			// The factory instance is re-useable and thread safe.
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setGZIPEnabled(true);
			cb.setIncludeRTsEnabled(true);
			cb.setIncludeMyRetweetEnabled(true);
			cb.setOAuthConsumerKey(context.getString(R.string.twitter_consumer_key_oye));
			cb.setOAuthConsumerSecret(context
					.getString(R.string.twitter_consumer_secret_oye));

			twitter = new TwitterFactory(cb.build()).getInstance();

			// Add user access token
			String at = settings.getString(Constants.PREFS_TWITTER_ACCESS_TOKEN, "");
			String ats = settings.getString(Constants.PREFS_TWITTER_ACCESS_TOKEN_SECRET,
					"");

			AccessToken a = new AccessToken(at, ats);
			twitter.setOAuthAccessToken(a);
		}

		@Override
		protected List<Tag3DView> doInBackground(Void... params) {
			int totalTweets = 100; // no of tweets to be fetched

			Paging paging = new Paging(1, totalTweets);

			List<twitter4j.Status> tweets = null;

			try {
				tweets = twitter.getHomeTimeline(paging);
			} catch (TwitterException e) {
				e.printStackTrace();
			}

			List<String> texts = new ArrayList<String>();

			if (tweets != null) {
				for (twitter4j.Status s : tweets) {
					texts.add(s.getText());
				}
			} else {
				//
			}

			// List<Tag3DView> myTagList =
			// generate3DCloud(generateCloud(texts));

			List<Tag3DView> myTagList = generate3DCloud(generateFakeCloud());

			return myTagList;
		}

		@Override
		protected void onPostExecute(List<Tag3DView> result) {
			super.onPostExecute(result);
			Log.w(Constants.TAG, "Tab 2 POS ====================");

			TextView tv = new TextViewRobotoSlab(getActivity());
			tv.setTextSize(18);
			FrameLayout.LayoutParams tvParams = new FrameLayout.LayoutParams(
					LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, Gravity.TOP);
			tvParams.setMargins(12, 12, 12, 0); // ltrb

			if (result == null) {
				tv.setText(getString(R.string.tab_summary_empty_twitter_others_cloud));
				rootLayout.addView(tv, tvParams);
				return;
			}

			// Create the 3D Tag Cloud view
			mTagCloudView = new TagCloud3DView(getActivity(), width, height, result);

			// setup views
			rootLayout
					.setBackgroundColor(getResources().getColor(R.color.background_app));
			mTagCloudView.requestFocus();
			mTagCloudView.setFocusableInTouchMode(true);

			// setup header textview
			tv.setText(getString(R.string.tab_summary_twitter_others_cloud_text));

			// add views to the fragment layout
			rootLayout.addView(mTagCloudView);
			rootLayout.addView(tv, tvParams);

			// hide indeterminate progress indicator
			mIndeterminateProgressBar.setVisibility(View.GONE);
		}

		private Tag3DView createTag(final String text, final int popularity,
				final int color) {
			final TagBundle bundle = new TagBundle(text, popularity, color);
			return new Tag3DView(getActivity(), bundle);
		}

		Comparator<Tag> comparator = new Comparator<Tag>() {
			@Override
			public int compare(Tag lhs, Tag rhs) {
				return lhs.getScoreInt() + rhs.getScoreInt();
			}
		};

		// public List<Tag> generateCloud(List<String> tweets) {
		// Cloud c = new Cloud();
		// c.setMaxTagsToDisplay(150);
		// c.setMaxWeight(50);
		// c.setMinWeight(46);
		// c.addOutputFilter(new MinLengthFilter(3));
		//
		// for (String s : tweets) {
		// c.addText(s);
		// }
		// return c.tags(comparator);
		// }

		public List<Tag3DView> generate3DCloud(List<Tag> cloud) {
			if (cloud.isEmpty())
				return null;

			List<Tag3DView> res = new ArrayList<Tag3DView>();
			int i = 0;
			int MAX_RESULTS = Math.min(cloud.size(), 24);

			Iterator<Tag> it = cloud.iterator();
			Matcher matcher = null;
			String digitRegex = "[0-9]+";
			Tag tag = null;
			boolean matched = false;

			while (it.hasNext() && i < MAX_RESULTS) {
				tag = it.next();
				String s = tag.getName();
				matched = false;

				if (!s.matches(digitRegex)) {
					for (Pattern p : Constants.PATTERNS_TWITTER_WORDS) {
						matcher = p.matcher(s);
						if (matcher.find()) {
							matched = true;
							break;
						}
					}
					if (!matched) {
						i++;
						res.add(createTag(tag.getName(), tag.getScoreInt(),
								R.color.background_left_panel));
					}
				}
			}
			return res;
		}

		public List<Tag> generateFakeCloud() {
			Cloud c = new Cloud();
			c.setMaxTagsToDisplay(12);
			c.setMaxWeight(36);
			c.setMinWeight(32);
			c.addOutputFilter(new MinLengthFilter(3));

			c.addText("messi messi messi messi messi messi");
			c.addText("cristiano cristiano cristiano cristiano");
			c.addText("verano verano");
			c.addText("humildat humildat humildat");
			c.addText("#breakingbad #breakingbad #breakingbad #breakingbad #breakingbad");
			c.addText("valors valors valors");
			c.addText("lamantabla lamantabla");
			c.addText("@eaudepep @eaudepep");
			c.addText("@eledelma @eledelma");
			c.addText("#apps #apps #apps #apps");
			c.addText("#truestory");
			c.addText("falete");

			return c.tags(comparator);
		}
	}
}

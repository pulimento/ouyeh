package es.pulimento.oye.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import es.pulimento.oye.misc.Constants;

public class MainDatabaseHelper extends SQLiteOpenHelper {

	private static MainDatabaseHelper mInstance = null;

	private static final String DATABASE_NAME = "1234.db";
	private static final int DATABASE_VERSION = 18;

	public static MainDatabaseHelper getInstance(Context context) {

		// Use the application context, which will ensure that you
		// don't accidentally leak an Activity's context.
		// See this article for more information: http://bit.ly/6LRzfx

		StackTraceElement[] stackTraceElements = Thread.currentThread()
				.getStackTrace();

		String s = "MainDatabaseHelper called!! " + "\n [3]"
				+ stackTraceElements[3].getClassName().toString() + " ## "
				+ stackTraceElements[3].getMethodName().toString() + "\n [4]"
				+ stackTraceElements[4].getClassName().toString() + " ## "
				+ stackTraceElements[4].getMethodName().toString();

		Log.i(Constants.TAG, s);

		if (mInstance == null) {
			mInstance = new MainDatabaseHelper(context.getApplicationContext());
		}
		return mInstance;
	}

	/**
	 * Constructor should be private to prevent direct instantiation. make call
	 * to static factory method "getInstance()" instead.
	 */
	private MainDatabaseHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Method is called during creation of the database
	@Override
	public void onCreate(SQLiteDatabase database) {
		ItemTable.onCreate(database);
		UserTable.onCreate(database);
		MockTable.onCreate(database);
		HeadsetPlugTable.onCreate(database);
		ActivityTable.onCreate(database);
		PositionTable.onCreate(database);
		CallLogTable.onCreate(database);
		TwitterTable.onCreate(database);
		CommentTable.onCreate(database);
		SongPlayedTable.onCreate(database);
	}

	// Method is called during an upgrade of the database,
	// e.g. if you increase the database version
	@Override
	public void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		ItemTable.onUpgrade(database, oldVersion, newVersion);
		UserTable.onUpgrade(database, oldVersion, newVersion);
		MockTable.onUpgrade(database, oldVersion, newVersion);
		HeadsetPlugTable.onUpgrade(database, oldVersion, newVersion);
		ActivityTable.onUpgrade(database, oldVersion, newVersion);
		PositionTable.onUpgrade(database, oldVersion, newVersion);
		CallLogTable.onUpgrade(database, oldVersion, newVersion);
		TwitterTable.onUpgrade(database, oldVersion, newVersion);
		CommentTable.onUpgrade(database, oldVersion, newVersion);
		SongPlayedTable.onUpgrade(database, oldVersion, newVersion);
	}

}

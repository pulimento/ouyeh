package es.pulimento.oye.bean;

import es.pulimento.oye.misc.Constants;

public class HeadsetEvent extends ItemEvent {

	private int _id;
	private int state;
	private String name;
	private int microphone;
	public final int type = Constants.TYPE_HEADSET_PLUG;

	public HeadsetEvent() {
	}

	public int getOwnIdItem() {
		return _id;
	}

	public void setOwnIdItem(int idItem) {
		this._id = idItem;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMicrophone() {
		return microphone;
	}

	public void setMicrophone(int microphone) {
		this.microphone = microphone;
	}

	@Override
	public String getField1() {
		return "Estado: " + (state == 1 ? "Conectado" : "Desconectado");
	}

	@Override
	public String getField2() {
		return "Tipo: " + name;
	}

	@Override
	public String getField3() {
		return (microphone == 1 ? "Con" : "Sin") + " micr�fono";
	}

	@Override
	public int getType() {
		return this.type;
	}

}

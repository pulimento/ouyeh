package es.pulimento.oye.dao;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import es.pulimento.oye.bean.SongPlayed;
import es.pulimento.oye.db.ItemTable;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.db.SongPlayedTable;
import es.pulimento.oye.misc.Constants;

public class DAOSongPlayed {

	public DAOSongPlayed() {
	}

	public Future<Long> insert(final Context context, final SongPlayed song) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {
				long id = -1;

				ContentValues cvItem = new ContentValues();
				cvItem.put(ItemTable.COLUMN_TIMESTAMP,
						System.currentTimeMillis());
				cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_SONG_PLAYED);

				MainDatabaseHelper dbHelper = MainDatabaseHelper
						.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();

				long idItem = db
						.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
				ContentValues values = new ContentValues();
				values.put(SongPlayedTable.COLUMN_ID_ITEM, idItem);
				values.put(SongPlayedTable.COLUMN_ALBUM, song.getSongAlbum());
				values.put(SongPlayedTable.COLUMN_ARTIST, song.getSongArtist());
				values.put(SongPlayedTable.COLUMN_COVER_URL, song.getCoverURL());
				values.put(SongPlayedTable.COLUMN_MISC, song.getMisc());
				values.put(SongPlayedTable.COLUMN_TITLE, song.getSongTitle());
				id = db.insert(SongPlayedTable.TABLE_NAME, "<empty>", values);

				db.close();

				return id;
			};
		};
		ScheduledExecutorService s = Executors
				.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

	public Future<Long> getNewerTimestamp(final Context context) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {

				MainDatabaseHelper dbHelper = MainDatabaseHelper
						.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();
				long newerTimestamp = -1;

				Cursor c = db.query(ItemTable.TABLE_NAME,
						new String[] { ItemTable.COLUMN_TIMESTAMP }, "type=?",
						new String[] { String
								.valueOf(Constants.TYPE_SONG_PLAYED) }, null,
						null, ItemTable.COLUMN_TIMESTAMP + " DESC", "1");

				if (c.moveToFirst()) {
					newerTimestamp = c.getLong(0);// Only one column
				}

				//db.close();

				return newerTimestamp;
			};
		};
		ScheduledExecutorService s = Executors
				.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

}

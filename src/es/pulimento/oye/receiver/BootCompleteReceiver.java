package es.pulimento.oye.receiver;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import es.pulimento.oye.AlwaysRunningService;
import es.pulimento.oye.misc.BaseReceiver;

public class BootCompleteReceiver extends BaseReceiver {

	private static final String OWN_NAME = "BootComplete";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();

		if (null == bundle)
			return;

		// Run always running service at boot complete
		Intent service = new Intent(context, AlwaysRunningService.class);
		if (!isMyServiceRunning(context))
			context.startService(service);

		potter(context, intent, OWN_NAME, true, null);
	}

	boolean isMyServiceRunning(Context context) {
		ActivityManager manager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (AlwaysRunningService.class.getName().equals(service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}
}
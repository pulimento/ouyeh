package es.pulimento.oye.thirdpartyapis.fb;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.ProfilePictureView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import es.pulimento.oye.R;
import es.pulimento.oye.misc.Constants;

public class FacebookActivity extends Activity {

	private TextView tv1, tv2, tv3, tv4, tv5;
	private ProfilePictureView ppv1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_fb);

		tv1 = (TextView) findViewById(R.id.tv_fb_1);
		tv2 = (TextView) findViewById(R.id.tv_fb_2);
		tv3 = (TextView) findViewById(R.id.tv_fb_3);
		tv4 = (TextView) findViewById(R.id.tv_fb_4);
		tv5 = (TextView) findViewById(R.id.tv_fb_5);
		ppv1 = (ProfilePictureView) findViewById(R.id.ppv_fb_1);

		((Button) findViewById(R.id.btn_fb_1))
				.setOnClickListener(new View.OnClickListener() {

					@Override
					public void onClick(View v) {
						Crouton.makeText(FacebookActivity.this,
								"Eres un malote, eh?", Style.CONFIRM).show();
					}
				});

		// start Facebook Login
		Session.openActiveSession(this, true, new Session.StatusCallback() {

			// callback when session changes state
			@Override
			public void call(Session session, SessionState state,
					Exception exception) {
				if (session.isOpened()) {
					// make request to the /me API
					Log.i(Constants.TAG,
							"Facebook session opened, " + session.toString());
					Request.executeMeRequestAsync(session,
							new Request.GraphUserCallback() {

								// callback after Graph API response with user
								// object
								@Override
								public void onCompleted(GraphUser user,
										Response response) {
									Log.i(Constants.TAG,
											"Facebook request completed, "
													+ user.toString() + " "
													+ response.toString());
									tv1.setText("Cumpleaņos: "
											+ user.getBirthday());
									tv2.setText("InnerJSON: "
											+ user.getInnerJSONObject()
													.toString());
									if (user.getLocation() != null)
										tv3.setText(user.getLocation()
												.toString());
									else
										tv3.setText("<no user location>");
									tv4.setText("FirstName: "
											+ user.getFirstName());
									tv5.setText("response string: "
											+ response.toString());
									ppv1.setProfileId(user.getId());
								}
							});
				}
			}
		});
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		Session.getActiveSession().onActivityResult(this, requestCode,
				resultCode, data);
		super.onActivityResult(requestCode, resultCode, data);
	}

}

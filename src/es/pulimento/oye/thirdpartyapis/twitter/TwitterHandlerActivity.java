package es.pulimento.oye.thirdpartyapis.twitter;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.User;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;
import es.pulimento.oye.R;
import es.pulimento.oye.dao.DAOUser;
import es.pulimento.oye.fetcher.TwitterFetcher;
import es.pulimento.oye.misc.Constants;

public class TwitterHandlerActivity extends Activity {

	RequestToken rt;
	private SharedPreferences settings;
	private Twitter twitter;
	public TextView tv;

	@Override
	protected void onActivityResult(final int requestCode,
			final int resultCode, final Intent data) {

		// Show -> Hey, you are logged in succesfully!! back to timeline
		// OR -> Horror!! Fail on sign in, go home, you are drunk

		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				if (resultCode == Activity.RESULT_OK) {
					if (requestCode == Constants.INTENT_SIGN_IN_TWITTER) {
						String oauthVerifier = (String) data.getExtras().get(
								"oauth_verifier");
						AccessToken at = null;
						try {
							if (rt == null || oauthVerifier == null) {
								runOnUiThread(new Runnable() {
									@Override
									public void run() {
										tv.setText("NULL RECEIVED!!");
									}
								});
								return;
							}
							at = twitter.getOAuthAccessToken(rt, oauthVerifier);

							// Save twitter tokens
							SharedPreferences.Editor prefEditor = settings
									.edit();
							prefEditor.putString(
									Constants.PREFS_TWITTER_ACCESS_TOKEN,
									at.getToken());
							prefEditor
									.putString(
											Constants.PREFS_TWITTER_ACCESS_TOKEN_SECRET,
											at.getTokenSecret());
							// Save login status
							prefEditor.putBoolean(
									Constants.PREFS_IS_LOGGED_IN_TWITTER, true);
							prefEditor.commit();

							fetchTwitterUserInfo();
							runOnUiThread(new Runnable() {
								@Override
								public void run() {
									tv.setText("todo OK");
								}
							});
							return;
						} catch (TwitterException e) {
							e.printStackTrace();
						}

					} else {
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								tv.setText("RequestCode != INTENT_SIGN_IN_TWITTER");
							}
						});
						return;
					}
				} else {
					runOnUiThread(new Runnable() {
						@Override
						public void run() {
							tv.setText("ResultCode != OK");
						}
					});
					return;
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						tv.setText("UNEXPECTED RESULT :(");
					}
				});
				return;
			};
		});

		t.start();

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Show -> you are going to integrate with twitter... blablabla
		// Se supone que si llega hasta aqu� es que no ha iniciado sesi�n
		// previamente, comprobar el logueo

		settings = PreferenceManager.getDefaultSharedPreferences(this);

		tv = new TextView(this);

		boolean isUserLogged = (settings.getString(
				Constants.PREFS_TWITTER_ACCESS_TOKEN, "")).length() > 0;

		if (!isUserLogged) {
			// The factory instance is re-useable and thread safe.
			ConfigurationBuilder cb = new ConfigurationBuilder();
			cb.setOAuthConsumerKey(getString(R.string.twitter_consumer_key_oye));
			cb.setOAuthConsumerSecret(getString(R.string.twitter_consumer_secret_oye));

			twitter = new TwitterFactory(cb.build()).getInstance();

			launchTwitterSignIn();
		} else {
			tv.setText("Ya se est� logueado, no deber�as estar aqu� ;)");
		}
		setContentView(tv);
	}

	public void fetchTwitterUserInfo() {

		new AsyncTask<Void, Void, User>() {

			@Override
			protected User doInBackground(Void... params) {

				try {
					long id = twitter.getId();
					User user = twitter.showUser(id);
					return user;
				} catch (IllegalStateException e) {
					e.printStackTrace();
				} catch (TwitterException e) {
					e.printStackTrace();
				}
				return null;
			}

			protected void onPostExecute(User user) {

				if (user != null) {
					SharedPreferences.Editor editor = settings.edit();
					editor.putString(Constants.PREFS_TWITTER_USERNAME,
							user.getScreenName());
					editor.commit();

					DAOUser d = new DAOUser(TwitterHandlerActivity.this);
					d.insertDetailsFromTwitter(user);

					new TwitterFetcher(TwitterHandlerActivity.this);
				} else {
					Log.e(Constants.TAG, "Error fetching twitter user info");
				}

			};
		}.execute();
	}

	private Future<Boolean> launchTwitterSignIn() {

		Callable<Boolean> c = new Callable<Boolean>() {

			@Override
			public Boolean call() throws Exception {

				try {
					rt = twitter
							.getOAuthRequestToken(getString(R.string.twitter_callback_oye));

				} catch (TwitterException e) {
					e.printStackTrace();
				}
				Intent intent = new Intent(TwitterHandlerActivity.this,
						TwitterSignInActivity.class);
				intent.putExtra("URL", rt.getAuthenticationURL());
				startActivityForResult(intent, Constants.INTENT_SIGN_IN_TWITTER);
				return true; // happy

			};
		};
		ScheduledExecutorService s = Executors
				.newSingleThreadScheduledExecutor();
		return s.submit(c);

	}

}

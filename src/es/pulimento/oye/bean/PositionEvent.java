package es.pulimento.oye.bean;

import java.text.DecimalFormat;

import es.pulimento.oye.misc.Constants;

public class PositionEvent extends ItemEvent {

	private int _id;
	private Double latitude;
	private Double longitude;
	private Float accuracy;
	public final int type = Constants.TYPE_POSITION;

	public PositionEvent() {
	}

	public int getOwnIdItem() {
		return _id;
	}

	public void setOwnIdItem(int idItem) {
		this._id = idItem;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Float getAccuracy() {
		return accuracy;
	}

	public void setAccuracy(Float accuracy) {
		this.accuracy = accuracy;
	}

	@Override
	public String getField1() {
		return "Latitud: " + String.valueOf(latitude);
	}

	@Override
	public String getField2() {
		return "Longitud: " + String.valueOf(longitude);
	}

	@Override
	public String getField3() {
		return "Precisión: " + String.valueOf(accuracy);
	}

	@Override
	public int getType() {
		return this.type;
	}

	public void roundDecimals(DecimalFormat df) {
		this.latitude = Double.valueOf(df.format(latitude));
		this.longitude = Double.valueOf(df.format(longitude));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result + ((longitude == null) ? 0 : longitude.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PositionEvent other = (PositionEvent) obj;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		return true;
	}

	// Implemented locally sensitive hashing

	// @Override
	// public int hashCode() {
	// final int prime = 31;
	// int result = 1;
	//
	// DecimalFormat df = new DecimalFormat("#.##");
	//
	// Double latitude1LHS = Double.valueOf(df.format(latitude));
	//
	// Double longitude1LHS = Double.valueOf(df.format(longitude));
	//
	// result = prime * result + ((latitude == null) ? 0 :
	// latitude1LHS.hashCode());
	// result = prime * result + ((longitude == null) ? 0 :
	// longitude1LHS.hashCode());
	// return result;
	// }
	//
	// @Override
	// public boolean equals(Object obj) {
	//
	// if (this == obj)
	// return true;
	// if (obj == null)
	// return false;
	// if (getClass() != obj.getClass())
	// return false;
	// PositionEvent other = (PositionEvent) obj;
	//
	// if (latitude == null) {
	// if (other.latitude != null)
	// return false;
	// }
	//
	// if (longitude == null) {
	// if (other.longitude != null)
	// return false;
	// }
	//
	// DecimalFormat df = new DecimalFormat("#.##");
	//
	// Double latitude1LHS = Double.valueOf(df.format(latitude));
	// Double latitude2LHS = Double.valueOf(df.format(other.latitude));
	// Double longitude1LHS = Double.valueOf(df.format(longitude));
	// Double longitude2LHS = Double.valueOf(df.format(other.longitude));
	//
	// if (!latitude1LHS.equals(latitude2LHS))
	// return false;
	//
	// if (!longitude1LHS.equals(longitude2LHS))
	// return false;
	// return true;
	// }

}

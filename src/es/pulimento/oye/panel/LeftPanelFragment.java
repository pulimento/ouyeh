package es.pulimento.oye.panel;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ToggleButton;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import es.pulimento.oye.MainActivity;
import es.pulimento.oye.R;
import es.pulimento.oye.TimelineActivity;
import es.pulimento.oye.misc.Constants;
import es.pulimento.oye.misc.MC;

public class LeftPanelFragment extends Fragment implements OnClickListener,
		OnCheckedChangeListener {

	IActivityCallback mCallback;
	private FrameLayout btnFilter, btnFilterByDay;
	private ImageView btnSearch;
	private ToggleButton tbPosition, tbTwitter;
	private SharedPreferences prefs;
	private Button btnSettings;

	public LeftPanelFragment() {

	}

	public interface IActivityCallback {
		// void updateLoader(Bundle b);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_panel_left,
				container, false);

		tbPosition = (ToggleButton) rootView
				.findViewById(R.id.tb_lmenu_filter_position);

		tbTwitter = (ToggleButton) rootView
				.findViewById(R.id.tb_lmenu_filter_twitter);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
		setupViews();
	}

	void setupViews() {
		// Gives value to fields

		btnFilter = (FrameLayout) getActivity().findViewById(
				R.id.btn_lmenu_show_filters);
		btnFilterByDay = (FrameLayout) getActivity().findViewById(
				R.id.btn_lmenu_filter_by_day);
		tbPosition.setChecked(prefs.getBoolean(
				Constants.PREFS_KEY_SHOW_POSITION, false));
		tbTwitter.setChecked(prefs.getBoolean(Constants.PREFS_KEY_SHOW_TWITTER,
				false));
		btnSettings = (Button) getActivity().findViewById(
				R.id.btn_lmenu_settings);
		btnSearch = (ImageView) getActivity().findViewById(R.id.btn_lmenu_search);

		// Set OnClickListeners...
		btnFilter.setOnClickListener(this);
		btnFilterByDay.setOnClickListener(this);
		btnSettings.setOnClickListener(this);
		btnSearch.setOnClickListener(this);

		// Set onChangeListeners...
		tbPosition.setOnCheckedChangeListener(this);
		tbTwitter.setOnCheckedChangeListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_lmenu_filter_by_day:
			startActivity(new Intent(getActivity(), MC.class));
			((TimelineActivity) getActivity()).toggleLeft();
			break;
		case R.id.btn_lmenu_show_filters:
			View toggles = getActivity().findViewById(
					R.id.ll_lmenu_container_toggle);
			if (toggles.getVisibility() == View.VISIBLE)
				toggles.setVisibility(View.GONE);
			else
				toggles.setVisibility(View.VISIBLE);
			break;
		case R.id.btn_lmenu_settings:
			startActivity(new Intent(getActivity(), MainActivity.class));
			((TimelineActivity) getActivity()).toggleLeft();
			break;
		case R.id.btn_lmenu_search:
			Crouton.makeText(getActivity(),"Buscando...", Style.INFO).show();
			((TimelineActivity) getActivity()).toggleLeft();
		default:
			break;
		}
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		Editor e = prefs.edit();
		switch (buttonView.getId()) {
		case R.id.tb_lmenu_filter_position:
			e.putBoolean(Constants.PREFS_KEY_SHOW_POSITION, isChecked);
			break;
		case R.id.tb_lmenu_filter_twitter:
			e.putBoolean(Constants.PREFS_KEY_SHOW_POSITION, isChecked);
			break;
		}
		e.commit();
	}

	// @Override
	// public void onAttach(Activity activity) {
	// super.onAttach(activity);
	//
	// // This makes sure that the container activity has implemented
	// // the callback interface. If not, it throws an exception
	// try {
	// mCallback = (IActivityCallback) activity;
	// } catch (ClassCastException e) {
	// throw new ClassCastException(activity.toString() +
	// " must implement IActivityCallback");
	// }
	// }

	// public void dosearch(String query, int positionSelected) {
	// // Highly coupled switch
	//
	// Bundle b = new Bundle();
	// if (positionSelected == 0) {
	// // Search by name
	// b.putString(Constants.LOADER_BUNDLE_ARGS_SELECTION,
	// MemberTable.COLUMN_NAME + " LIKE '"
	// + query + "'");
	// } else if (positionSelected == 1) {
	// // Search by division
	// b.putString(Constants.LOADER_BUNDLE_ARGS_SELECTION,
	// MemberTable.COLUMN_DIVISION
	// + " LIKE '" + query + "'");
	// }
	// mCallback.updateLoader(b);
	// }
}

package es.pulimento.oye.fetcher;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import es.pulimento.oye.misc.Constants;

public class MainFetcher {

	public MainFetcher(Context context) {

		SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(context);

		boolean fetchCallLog = true;
		boolean isTwitterUserLogged = (settings.getString(Constants.PREFS_TWITTER_ACCESS_TOKEN, ""))
				.length() > 0;

		if (fetchCallLog) {
			new CallLogFetcher(context);
		}

		if (isTwitterUserLogged) {
			new TwitterFetcher(context).defaultFetchToDB(context);
		}

	}

}

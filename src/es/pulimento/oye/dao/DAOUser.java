package es.pulimento.oye.dao;

import twitter4j.User;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.db.UserTable;
import es.pulimento.oye.misc.Constants;

public class DAOUser {

	private Context c;

	public DAOUser(Context context) {
		this.c = context;
	}

	public/* List< */es.pulimento.oye.bean.User/* > */getUser() {
		SQLiteDatabase db = MainDatabaseHelper.getInstance(c)
				.getReadableDatabase();
		Cursor cursor = db.query(true, UserTable.TABLE_NAME, null, null, null,
				null, null, null, null);

		//List<es.pulimento.oye.bean.User> users = null;
		if (cursor.moveToFirst()) {
			// users = new ArrayList<es.pulimento.oye.bean.User>();
			// do {
			es.pulimento.oye.bean.User u = new es.pulimento.oye.bean.User();
			u.set_id(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_ID)));
			u.setGoogle_id(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_GG_ID)));
			u.setFamily_name(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_GG_FAMILY_NAME)));
			u.setGiven_name(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_GG_GIVEN_NAME)));
			u.setGender(cursor.getInt(cursor
					.getColumnIndex(UserTable.COLUMN_GG_GENDER)));
			u.setLocale(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_GG_LOCALE)));
			u.setPicture(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_GG_PICTURE)));
			u.setTw_description(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_TW_DESCRIPTION)));
			u.setTw_followers(cursor.getInt(cursor
					.getColumnIndex(UserTable.COLUMN_TW_FOLLOWERS)));
			u.setTw_friends(cursor.getInt(cursor
					.getColumnIndex(UserTable.COLUMN_TW_FRIENDS)));
			u.setTw_id(cursor.getInt(cursor
					.getColumnIndex(UserTable.COLUMN_TW_ID)));
			u.setTw_locale(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_TW_LOCALE)));
			u.setTw_location(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_TW_LOCATION)));
			u.setTw_name(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_TW_NAME)));
			u.setTw_number_of_tweets(cursor.getInt(cursor
					.getColumnIndex(UserTable.COLUMN_TW_NUMBER_OF_TWEETS)));
			u.setTw_picture(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_TW_PICTURE)));
			u.setTw_screen_name(cursor.getString(cursor
					.getColumnIndex(UserTable.COLUMN_TW_SCREEN_NAME)));
			u.setTw_since(cursor.getInt(cursor
					.getColumnIndex(UserTable.COLUMN_TW_SINCE)));
			// users.add(u);
			// } while (cursor.moveToNext());
			// db.close();
			return u;
		}
		return null;
	}

	public boolean isLoggedOnTwitter() {
		return getUser() != null ? getUser().getTw_id() > 0 : false;
	}

	public boolean insertFromGoogle(es.pulimento.oye.bean.User u) {

		if (u == null) {
			Log.e(Constants.TAG, "tried to insert null user");
			return false;
		}

		ContentValues cv = new ContentValues();
		cv.put(UserTable.COLUMN_GG_ID, u.getGoogle_id());
		cv.put(UserTable.COLUMN_GG_FAMILY_NAME, u.getFamily_name());
		cv.put(UserTable.COLUMN_GG_GENDER, u.getGender());
		cv.put(UserTable.COLUMN_GG_GIVEN_NAME, u.getGiven_name());
		cv.put(UserTable.COLUMN_GG_LOCALE, u.getLocale());
		cv.put(UserTable.COLUMN_GG_PICTURE, u.getPicture());

		SQLiteDatabase db = MainDatabaseHelper.getInstance(c)
				.getWritableDatabase();
		db.insertOrThrow(UserTable.TABLE_NAME, "nch", cv);
		// db.close();

		return true;
	}

	public boolean insertDetailsFromTwitter(User user) {
		if (user == null)
			return false;

		ContentValues cv = new ContentValues();
		cv.put(UserTable.COLUMN_TW_DESCRIPTION, user.getDescription());
		cv.put(UserTable.COLUMN_TW_FOLLOWERS, user.getFollowersCount());
		cv.put(UserTable.COLUMN_TW_FRIENDS, user.getFriendsCount());
		cv.put(UserTable.COLUMN_TW_ID, user.getId());
		cv.put(UserTable.COLUMN_TW_LOCALE, user.getLang());
		cv.put(UserTable.COLUMN_TW_LOCATION, user.getLocation());
		cv.put(UserTable.COLUMN_TW_NAME, user.getName());
		cv.put(UserTable.COLUMN_TW_NUMBER_OF_TWEETS, user.getStatusesCount());
		cv.put(UserTable.COLUMN_TW_PICTURE, user.getBiggerProfileImageURL());
		cv.put(UserTable.COLUMN_TW_SCREEN_NAME, user.getScreenName());
		cv.put(UserTable.COLUMN_TW_SINCE, user.getCreatedAt().getTime());

		SQLiteDatabase db = MainDatabaseHelper.getInstance(c)
				.getWritableDatabase();
		db.updateWithOnConflict(UserTable.TABLE_NAME, cv, null, null, 0);
		Log.i(Constants.TAG, "Twitter user details inserted into database");

		// db.close();

		return true;

	}

	public int getUsersCount() {
		SQLiteDatabase db = MainDatabaseHelper.getInstance(c)
				.getWritableDatabase();
		Cursor mCount = db.rawQuery("select count(*) from "
				+ UserTable.TABLE_NAME, null);
		mCount.moveToFirst();
		int count = mCount.getInt(0);
		mCount.close();
		return count;
	}
}

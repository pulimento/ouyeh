package es.pulimento.oye.receiver;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import es.pulimento.oye.dao.DAOHeadset;
import es.pulimento.oye.db.HeadsetPlugTable;
import es.pulimento.oye.misc.BaseReceiver;

public class HeadsetPlugReceiver extends BaseReceiver {

	private static final String OWN_NAME = "Headset plug";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();

		if (null == bundle)
			return;

		ContentValues cv = new ContentValues();
		cv.put(HeadsetPlugTable.COLUMN_MICROPHONE, bundle.getInt("microphone"));
		cv.put(HeadsetPlugTable.COLUMN_STATE, bundle.getInt("state"));
		cv.put(HeadsetPlugTable.COLUMN_NAME, bundle.getString("name"));
		DAOHeadset d = new DAOHeadset();
		d.insert(context, cv);

		String state = "Se ha "
				+ (bundle.getInt("state") == 1 ? "conectado" : "desconectado");
		state += bundle.getString("name") + " ";
		state += ", " + (bundle.getInt("microphone") == 0 ? "SI" : "NO")
				+ " tiene micr�fono";

		potter(context, intent, OWN_NAME, true, state);
	}
}
package es.pulimento.oye;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ToggleButton;
import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import es.pulimento.oye.bean.GeneratedActivity;
import es.pulimento.oye.dao.DAOItem;
import es.pulimento.oye.dao.DAOPosition;
import es.pulimento.oye.dao.DAOTwitter;
import es.pulimento.oye.db.ActivityTable;
import es.pulimento.oye.db.PositionTable;
import es.pulimento.oye.fetcher.ActivityGenerator;
import es.pulimento.oye.fetcher.TwitterFetcher;
import es.pulimento.oye.misc.Constants;
import es.pulimento.oye.summary.SummaryTwitterActivity;

public class MocksActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		LinearLayout ll = new LinearLayout(MocksActivity.this);
		ll.setOrientation(LinearLayout.VERTICAL);

		LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		params.setMargins(10, 30, 10, 10);
		ll.setLayoutParams(params);

		Button b3 = new Button(this);
		b3.setText("Insert generated activity");
		b3.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				GeneratedActivity g = ActivityGenerator.generate();
				ContentValues cv = new ContentValues();
				cv.put(ActivityTable.COLUMN_ACTIVITY, g.getActivity());
				cv.put(ActivityTable.COLUMN_NAME, g.getName());
				cv.put(ActivityTable.COLUMN_PERSON, g.getPerson());
				// getContentResolver().insert(ActivityProvider.CONTENT_URI_ACTIVITY,
				// cv);
				Crouton.makeText(MocksActivity.this, "Simulated activity inserted!",
						Style.CONFIRM).show();
			}
		});
		ll.addView(b3);

		Button b4 = new Button(this);
		b4.setText("Insert position");
		b4.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				ContentValues cv = new ContentValues();
				cv.put(PositionTable.COLUMN_ACCURACY, 69);
				cv.put(PositionTable.COLUMN_LATITUDE, 69);
				cv.put(PositionTable.COLUMN_LONGITUDE, 69);
				DAOPosition d = new DAOPosition();
				d.insert(MocksActivity.this, cv);
				Crouton.makeText(MocksActivity.this, "Position inserted!", Style.CONFIRM)
						.show();
			}
		});
		ll.addView(b4);

		Button b5 = new Button(this);
		b5.setText("ITEMDAO getItems()");
		b5.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				DAOItem d = new DAOItem();
				try {
					d.getItems(MocksActivity.this, null, 0).get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
				Log.i(Constants.TAG, "Hell yeah!!");
			}
		});
		ll.addView(b5);

		Button b6 = new Button(this);
		b6.setText("Twitter re-fetch");
		b6.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				DAOTwitter d = new DAOTwitter();
				d.deleteAllTweetsUSEWITHCAUTION(true, MocksActivity.this);
				new TwitterFetcher(MocksActivity.this);
				Log.i(Constants.TAG, "Hell yeah!!");
			}
		});
		ll.addView(b6);

		Button b8 = new Button(this);
		b8.setText("Tag cloud");
		b8.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MocksActivity.this,
						SummaryTwitterActivity.class);
				startActivity(intent);
			}
		});
		ll.addView(b8);

		ToggleButton showPosition = new ToggleButton(this);
		showPosition.setText("Show position items on TL");
		SharedPreferences s = PreferenceManager
				.getDefaultSharedPreferences(MocksActivity.this);
		showPosition.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				SharedPreferences s = PreferenceManager
						.getDefaultSharedPreferences(MocksActivity.this);
				Editor e = s.edit();
				e.putBoolean(Constants.PREFS_KEY_SHOW_POSITION, isChecked);
				e.commit();
			}
		});
		boolean mustBeChecked = s.getBoolean(Constants.PREFS_KEY_SHOW_POSITION, false);
		showPosition.setChecked(mustBeChecked);
		ll.addView(showPosition);

		setContentView(ll);
	}
}

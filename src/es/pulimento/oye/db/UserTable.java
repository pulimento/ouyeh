package es.pulimento.oye.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import es.pulimento.oye.misc.Constants;

public class UserTable {

	// Database table
	public static final String TABLE_NAME = "user";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_GG_ID = "google_id";
	public static final String COLUMN_GG_GIVEN_NAME = "gg_given_name";
	public static final String COLUMN_GG_FAMILY_NAME = "gg_family_name";
	public static final String COLUMN_GG_GENDER = "gg_gender";
	public static final String COLUMN_GG_LOCALE = "gg_locale";
	public static final String COLUMN_GG_PICTURE = "gg_picture";
	public static final String COLUMN_TW_ID = "tw_id";
	public static final String COLUMN_TW_PICTURE = "tw_picture";
	public static final String COLUMN_TW_SINCE = "tw_since";
	public static final String COLUMN_TW_DESCRIPTION = "tw_description";
	public static final String COLUMN_TW_FOLLOWERS = "tw_followers";
	public static final String COLUMN_TW_FRIENDS = "tw_friends";
	public static final String COLUMN_TW_LOCALE = "tw_locale";
	public static final String COLUMN_TW_LOCATION = "tw_location";
	public static final String COLUMN_TW_NAME = "tw_name";
	public static final String COLUMN_TW_SCREEN_NAME = "tw_screen_name";
	public static final String COLUMN_TW_NUMBER_OF_TWEETS = "tw_tweets_count";

	// Database creation SQLite statement
	public static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_NAME + "(" + COLUMN_ID + " integer primary key autoincrement,"
			+ COLUMN_GG_ID + " text, " + COLUMN_GG_GENDER + " integer,"
			+ COLUMN_GG_LOCALE + " text," + COLUMN_GG_PICTURE + " text,"
			+ COLUMN_GG_GIVEN_NAME + " text, " + COLUMN_GG_FAMILY_NAME + " text,"
			+ COLUMN_TW_ID + " integer," + COLUMN_TW_PICTURE + " text," + COLUMN_TW_SINCE
			+ " integer," + COLUMN_TW_DESCRIPTION + " text," + COLUMN_TW_FOLLOWERS
			+ " integer," + COLUMN_TW_FRIENDS + " integer," + COLUMN_TW_LOCALE + " text,"
			+ COLUMN_TW_LOCATION + " text," + COLUMN_TW_NAME + " text,"
			+ COLUMN_TW_SCREEN_NAME + " text," + COLUMN_TW_NUMBER_OF_TWEETS
			+ " integer);";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		Log.w(Constants.TAG, "Upgrading database from version " + oldVersion + " to "
				+ newVersion + ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
		onCreate(database);
	}

}

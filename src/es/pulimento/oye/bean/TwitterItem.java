package es.pulimento.oye.bean;

import es.pulimento.oye.misc.Constants;

public class TwitterItem extends ItemEvent {

	private int _id;
	public static final int type = Constants.TYPE_TWITTER;
	private long fetchedAt;
	private String geolocation;
	private String place;
	private int rtCount;
	private String source;
	private String text;
	private String user;
	private long twitterID;
	private int tweetType;

	public static final int TYPE_USER_TIMELINE = 0;
	public static final int TYPE_USER_RETWEET = 1;
	public static final int TYPE_USER_MENTION = 2;

	public int getTweetType() {
		return tweetType;
	}

	public void setTweetType(int tweetType) {
		this.tweetType = tweetType;
	}

	public long getTwitterID() {
		return twitterID;
	}

	public void setTwitterID(long twitterID) {
		this.twitterID = twitterID;
	}

	public TwitterItem() {
	}

	public long getFetchedAt() {
		return fetchedAt;
	}

	public void setFetchedAt(long fetchedAt) {
		this.fetchedAt = fetchedAt;
	}

	public String getGeolocation() {
		return geolocation;
	}

	public void setGeolocation(String geolocation) {
		this.geolocation = geolocation;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public int getRtCount() {
		return rtCount;
	}

	public void setRtCount(int rtCount) {
		this.rtCount = rtCount;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public int get_id() {
		return _id;
	}

	public int getType() {
		return type;
	}

	@Override
	public String getField1() {
		return text;
	}

	@Override
	public String getField2() {
		return user;
	}

	@Override
	public String getField3() {
		return source;
	}

}

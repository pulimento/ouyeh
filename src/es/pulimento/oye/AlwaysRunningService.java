package es.pulimento.oye;

import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationRequest;

import es.pulimento.oye.bean.GeneratedActivity;
import es.pulimento.oye.dao.DAOActivity;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.db.MockTable;
import es.pulimento.oye.fetcher.ActivityGenerator;
import es.pulimento.oye.fetcher.MainFetcher;
import es.pulimento.oye.misc.Constants;
import es.pulimento.oye.receiver.HeadsetPlugReceiver;
import es.pulimento.oye.receiver.MusicReceiver;

public class AlwaysRunningService extends Service implements
		GooglePlayServicesClient.ConnectionCallbacks,
		GooglePlayServicesClient.OnConnectionFailedListener {

	// TODO partial wakelock here

	BroadcastReceiver mScreenReceiver, mHeadsetReceiver, mMusicReceiver;

	public static final int POSITION_REQUEST_INTERVAL = 1000 * 60 * 60;
	public static final int INTERVAL_ACTIVITY_GENERATOR = 911 * 60 * 60 * 5;
	private Handler activityGeneratorHandler;
	private LocationClient locationclient;
	private LocationRequest mLocationRequest;
	private PendingIntent mPendingIntent;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(Constants.TAG, "AlwaysRunningService onStartCommand");
		super.onStartCommand(intent, flags, startId);

		// Screen receiver
		// IntentFilter intentFilterScreen = new IntentFilter();
		// intentFilterScreen.addAction("android.intent.action.SCREEN_OFF");
		// intentFilterScreen.addAction("android.intent.action.SCREEN_ON");
		// mScreenReceiver = new ScreenReceiver();
		// registerReceiver(mScreenReceiver, intentFilterScreen);

		// ============== Headset receiver ===============
		mHeadsetReceiver = new HeadsetPlugReceiver();
		registerReceiver(mHeadsetReceiver, new IntentFilter(
				Intent.ACTION_HEADSET_PLUG));

		// ============== Music receiver =================
		IntentFilter intentFilterMusic = new IntentFilter();
		// Stock android
		intentFilterMusic.addAction("com.android.music.metachanged");
		intentFilterMusic.addAction("com.android.music.playstatechanged");
		intentFilterMusic.addAction("com.android.music.playbackcomplete");
		intentFilterMusic.addAction("com.android.music.queuechanged");
		// Soundcloud
		intentFilterMusic.addAction("com.soundcloud.android.metachanged");
		intentFilterMusic.addAction("com.soundcloud.android.playstatechanged");
		intentFilterMusic.addAction("com.soundcloud.android.playbackcomplete");
		intentFilterMusic.addAction("com.soundcloud.android.queuechanged");
		// Google Play Music
		intentFilterMusic.addAction("com.google.android.music.metachanged");
		intentFilterMusic
				.addAction("com.google.android.music.playstatechanged");
		intentFilterMusic
				.addAction("com.google.android.music.playbackcomplete");
		intentFilterMusic.addAction("com.google.android.music.queuechanged");
		intentFilterMusic.addAction("com.google.android.music.metachanged");
		// Others
		intentFilterMusic.addAction("com.htc.music.metachanged");
		intentFilterMusic.addAction("fm.last.android.metachanged");
		intentFilterMusic.addAction("com.sec.android.app.music.metachanged");
		intentFilterMusic.addAction("com.nullsoft.winamp.metachanged");
		intentFilterMusic.addAction("com.amazon.mp3.metachanged");
		intentFilterMusic.addAction("com.miui.player.metachanged");
		intentFilterMusic.addAction("com.real.IMP.metachanged");
		intentFilterMusic.addAction("com.sonyericsson.music.metachanged");
		intentFilterMusic.addAction("com.rdio.android.metachanged");
		intentFilterMusic
				.addAction("com.samsung.sec.android.MusicPlayer.metachanged");
		intentFilterMusic.addAction("com.andrew.apollo.metachanged");

		mMusicReceiver = new MusicReceiver();
		registerReceiver(mMusicReceiver, intentFilterMusic);

		SQLiteDatabase db = MainDatabaseHelper.getInstance(
				AlwaysRunningService.this).getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(MockTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
		cv.put(MockTable.COLUMN_INTENT, "AlwaysRunningService");
		cv.put(MockTable.COLUMN_DATA_1, "onStartCommand");
		db.insertOrThrow(MockTable.TABLE_MOCK, "nch", cv);

		return START_STICKY;// Let's service run always
	}

	@Override
	public void onCreate() {
		super.onCreate();

		// Log and store in DB that the service has started
		Log.i(Constants.TAG, "AlwaysRunningService onCreate");

		SQLiteDatabase db = MainDatabaseHelper.getInstance(
				AlwaysRunningService.this).getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(MockTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
		cv.put(MockTable.COLUMN_INTENT, "AlwaysRunningService");
		cv.put(MockTable.COLUMN_DATA_1, "onCreate");
		db.insertOrThrow(MockTable.TABLE_MOCK, "nch", cv);

		// Start generating activities
		activityGeneratorHandler = new Handler();
		startRepeatingTask();

		// Setup and initialize location updates from fused location provider
		locationclient = new LocationClient(this, this, this);
		locationclient.connect();
		Intent intentService = new Intent(this, LocationService.class);
		mPendingIntent = PendingIntent.getService(this, 1, intentService, 0);
		mLocationRequest = LocationRequest.create();
		mLocationRequest.setInterval(POSITION_REQUEST_INTERVAL);
		mLocationRequest
				.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);

		// Start fetcher to retrieve fresh content
		new MainFetcher(this);
		Log.i(Constants.TAG, "Main fetcher started!!");
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		// Log and store in DB that the service has stopped
		Log.i(Constants.TAG, "AlwaysRunningService onDestroy");

		SQLiteDatabase db = MainDatabaseHelper.getInstance(
				AlwaysRunningService.this).getWritableDatabase();
		ContentValues cv = new ContentValues();
		cv.put(MockTable.COLUMN_TIMESTAMP, System.currentTimeMillis());
		cv.put(MockTable.COLUMN_INTENT, "AlwaysRunningService");
		cv.put(MockTable.COLUMN_DATA_1, "onDestroy");
		db.insertOrThrow(MockTable.TABLE_MOCK, "nch", cv);

		// Unregister receivers and stop location updates
		unregisterReceiver(mScreenReceiver);
		unregisterReceiver(mHeadsetReceiver);
		unregisterReceiver(mMusicReceiver);
		stopRepeatingTask();
		locationclient.disconnect();
	}

	// ================= Activity generator stuff ================

	Runnable activityGeneratorRunnable = new Runnable() {
		@Override
		public void run() {
			GeneratedActivity g = ActivityGenerator.generate();
			DAOActivity daoa = new DAOActivity();
			daoa.insert(AlwaysRunningService.this, g);
			activityGeneratorHandler.postDelayed(activityGeneratorRunnable,
					INTERVAL_ACTIVITY_GENERATOR);
		}
	};

	void startRepeatingTask() {
		activityGeneratorRunnable.run();
	}

	void stopRepeatingTask() {
		activityGeneratorHandler.removeCallbacks(activityGeneratorRunnable);
	}

	// ======= Google Play Services Location Provider callbacks =====

	@Override
	public void onConnectionFailed(ConnectionResult arg0) {
		Log.e(Constants.TAG, "Fused location provider, CONNECTION FAILED!!");
	}

	@Override
	public void onConnected(Bundle arg0) {
		Log.i(Constants.TAG, "Fused location provider, CONNECTED SUCCESFULLY");
		locationclient.requestLocationUpdates(mLocationRequest, mPendingIntent);
	}

	@Override
	public void onDisconnected() {
		Log.e(Constants.TAG, "Fused location provider DISCONNECTED!");
	}

}

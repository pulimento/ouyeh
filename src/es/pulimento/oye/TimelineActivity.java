package es.pulimento.oye;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;

import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshAttacher;
import android.app.ActionBar;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningServiceInfo;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.fortysevendeg.android.swipelistview.BaseSwipeListViewListener;
import com.fortysevendeg.android.swipelistview.SwipeListView;

import de.keyboardsurfer.android.widget.crouton.Crouton;
import de.keyboardsurfer.android.widget.crouton.Style;
import es.pulimento.oye.adapter.ItemAdapter;
import es.pulimento.oye.bean.CallLogItem;
import es.pulimento.oye.bean.GeneratedActivity;
import es.pulimento.oye.bean.ItemEvent;
import es.pulimento.oye.bean.SongPlayed;
import es.pulimento.oye.bean.TwitterItem;
import es.pulimento.oye.dao.DAOItem;
import es.pulimento.oye.misc.Constants;
import es.pulimento.oye.misc.customview.TextViewSanchez;

public class TimelineActivity extends FragmentActivity implements
		PullToRefreshAttacher.OnRefreshListener {

	// TODO move action bar layout definition to XML

	// TODO add new items on top
	// http://stackoverflow.com/questions/5903597/add-new-items-to-top-of-list-view-on-android

	// TODO MAXIMUM PRIORITY fix refreshers

	private List<ItemEvent> data;
	private ItemAdapter adapter;
	private SwipeListView swipeListView;
	private PullToRefreshAttacher mPullToRefreshAttacher;
	private DrawerLayout mDrawerLayout;
	private ActionBarDrawerToggle mDrawerToggle;
	private int mFirstVisibleItem;
	private boolean mLastScrollingUp;
	private TextViewSanchez abTitle;
	private ImageView abLeft, abRight;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_timeline);

		// Start always running service
		Intent service = new Intent(this, AlwaysRunningService.class);
		if (!isMyServiceRunning())
			startService(service);

		// Preparing adapter...
		data = new ArrayList<ItemEvent>();
		adapter = new ItemAdapter(this, data);
		mFirstVisibleItem = -1;
		mLastScrollingUp = true;

		// Swipe list view stuff...
		swipeListView = (SwipeListView) findViewById(R.id.slv_timeline);
		setSLVchoiceModeAndMultiSelect();
		swipeListView.setAdapter(adapter);
		setupSwipeListView();

		// Pull to refresh stuff...
		mPullToRefreshAttacher = new PullToRefreshAttacher(this);
		// Set the Refreshable View to be the ListView and the refresh listener
		// to be this.
		mPullToRefreshAttacher.setRefreshableView(swipeListView, this);

		// Delegate OnTouch calls to both libraries that want to receive them...
		swipeListView.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View view, MotionEvent motionEvent) {
				if (!swipeListView.getTouchListener().onTouch(view, motionEvent)) {
					// Only allow pull to refresh if not swiping list item
					mPullToRefreshAttacher.onTouch(view, motionEvent);
				}
				return false;
			}
		});

		// Navigation drawer...
		setupDrawerLayoutAndDrawerToggle();

		// Action bar...
		loadCustomActionBar();

		// Populate list...
		new PopulateListTask(null).execute();
	}

	@Override
	protected void onResume() {
		super.onResume();

		SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
		boolean isLogged = prefs.getBoolean(Constants.PREFS_IS_LOGGED_IN_TWITTER, false);
		View login = findViewById(R.id.btn_rmenu_twitter_login);
		View stats = findViewById(R.id.btn_rmenu_twitter_stats);
		if (isLogged) {
			login.setVisibility(View.GONE);
			stats.setVisibility(View.VISIBLE);
		} else {
			login.setVisibility(View.VISIBLE);
			stats.setVisibility(View.GONE);
		}
	}

	@Override
	public void onRefreshStarted(View view) {

		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				new PopulateListTask(true).execute();
				return null;
			}

			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);

				// Notify PullToRefreshAttacher that the refresh has finished
				mPullToRefreshAttacher.setRefreshComplete();
			}
		}.execute();
	}

	public class PopulateListTask extends AsyncTask<Boolean, Void, List<ItemEvent>> {

		Boolean fetchNewerItems;

		public PopulateListTask(Boolean getNewerItems) {
			this.fetchNewerItems = getNewerItems;
		}

		protected List<ItemEvent> doInBackground(Boolean... args) {
			List<ItemEvent> data = new ArrayList<ItemEvent>();
			DAOItem ied = new DAOItem();
			try {
				if (fetchNewerItems != null) {
					if (fetchNewerItems) {
						data = ied.getItems(TimelineActivity.this, true,
								adapter.getFirstTimestamp()).get();
					} else {
						data = ied.getItems(TimelineActivity.this, false,
								adapter.getLastTimestamp()).get();
					}
				} else {
					// Defaults, load N first items
					data = ied.getItems(TimelineActivity.this, null, 0).get();
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}

			return data;
		}

		protected void onPostExecute(List<ItemEvent> result) {
			if (result != null) {
				String s = "";
				if (fetchNewerItems != null) {
					s += "No se borra lo existente, a�adidos " + result.size() + " items";
				} else {
					data.clear();
					s += "Cargados " + result.size() + " items";
				}
				Crouton.makeText(TimelineActivity.this, s, Style.CONFIRM).show();
				data.addAll(result);
				adapter.notifyDataSetChanged();
			} else {
				Crouton.makeText(TimelineActivity.this,
						"No hay m�s elementos que a�adir", Style.INFO).show();
			}
		}
	}

	public class FakePopulateListTask extends AsyncTask<Boolean, Void, List<ItemEvent>> {

		public FakePopulateListTask(Boolean putHereAnything) {
		}

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			data.clear();
			adapter.notifyDataSetChanged();
		}

		protected List<ItemEvent> doInBackground(Boolean... args) {
			List<ItemEvent> data = new ArrayList<ItemEvent>();

			Log.i(Constants.TAG, "fake populating list... ");

			CallLogItem c1 = new CallLogItem();
			c1.setTimestamp(System.currentTimeMillis() - 1000 * 60 * 2);
			c1.setCallType(CallLogItem.TYPE_INCOMING);
			c1.setName("Pablo Salas");
			c1.setNumber("635214657");
			data.add(c1);

			SongPlayed s1 = new SongPlayed();
			s1.setSongTitle("The Other Side");
			s1.setSongAlbum("In silico");
			s1.setSongArtist("Pendulum");
			s1.setTimestamp(System.currentTimeMillis() - 1000 * 60 * 4);
			data.add(s1);

			SongPlayed s2 = new SongPlayed();
			s2.setSongTitle("Propane Nightmares");
			s2.setSongAlbum("In silico");
			s2.setSongArtist("Pendulum");
			s2.setTimestamp(System.currentTimeMillis() - 1000 * 60 * 8);
			data.add(s2);

			SongPlayed s3 = new SongPlayed();
			s3.setSongTitle("U Can't Touch This");
			s3.setSongAlbum("Please Hammer Don't Hurt Them");
			s3.setSongArtist("MC Hammer");
			s3.setTimestamp(System.currentTimeMillis() - 1000 * 60 * 11);
			data.add(s3);

			GeneratedActivity a1 = new GeneratedActivity();
			a1.setActivity("tomando una cerveza");
			a1.setName("Pedro P�rez");
			a1.setPerson("Pedro P�rez");
			a1.setTimestamp(System.currentTimeMillis() - 1000 * 60 * 58);
			data.add(a1);

			TwitterItem t1 = new TwitterItem();
			t1.setText("Nos tomamos algo?");
			t1.setTweetType(TwitterItem.TYPE_USER_MENTION);
			t1.setUser("@bignoserabbit");
			t1.setTimestamp(System.currentTimeMillis() - 1000 * 60 * 116);
			data.add(t1);

			SongPlayed s4 = new SongPlayed();
			s4.setSongTitle("Tearin' it up");
			s4.setSongAlbum("Street Bangerz");
			s4.setSongArtist("Gramatik");
			s4.setTimestamp(System.currentTimeMillis() - 1000 * 60 * 185);
			data.add(s4);

			TwitterItem t2 = new TwitterItem();
			t2.setText("is there a barber service that will come cut my beard while i keep writing code?");
			t2.setTweetType(TwitterItem.TYPE_USER_RETWEET);
			t2.setUser("@koush");
			t2.setTimestamp(System.currentTimeMillis() - 1000 * 60 * 320);
			data.add(t2);

			return data;
		}

		protected void onPostExecute(List<ItemEvent> result) {
			if (result != null) {
				data.addAll(result);
				adapter.notifyDataSetChanged();
			} else {
				Crouton.makeText(TimelineActivity.this,
						"No hay m�s elementos que a�adir", Style.INFO).show();
			}
		}
	}

	// =================================================================
	// =============== Utility and misc methods ========================
	// =================================================================

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	boolean isMyServiceRunning() {
		ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
		for (RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
			if (AlwaysRunningService.class.getName().equals(
					service.service.getClassName())) {
				return true;
			}
		}
		return false;
	}

	public boolean toggleLeft() {
		View vl = findViewById(R.id.left_drawer);
		View vr = findViewById(R.id.right_drawer);
		if (mDrawerLayout.isDrawerOpen(vl)) {
			mDrawerLayout.closeDrawer(vl);
			return false;
		} else {
			mDrawerLayout.openDrawer(vl);
			if (mDrawerLayout.isDrawerOpen(vr)) {
				mDrawerLayout.closeDrawer(vr);
			}
			return true;
		}
	}

	public boolean toggleRight() {
		View vl = findViewById(R.id.left_drawer);
		View vr = findViewById(R.id.right_drawer);
		if (mDrawerLayout.isDrawerOpen(vr)) {
			mDrawerLayout.closeDrawer(vr);
			return false;
		} else {
			mDrawerLayout.openDrawer(vr);
			if (mDrawerLayout.isDrawerOpen(vl)) {
				mDrawerLayout.closeDrawer(vl);
			}
			return true;
		}
	}

	private void setupSwipeListView() {
		swipeListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		swipeListView.setSwipeMode(SwipeListView.SWIPE_MODE_BOTH);
		swipeListView.setSwipeActionRight(SwipeListView.SWIPE_ACTION_CHOICE);
		swipeListView.setSwipeActionLeft(SwipeListView.SWIPE_ACTION_REVEAL);
		swipeListView.setOffsetLeft(20f);
		swipeListView.setOffsetRight(20f);
		swipeListView.setSwipeOpenOnLongPress(true);
		swipeListView.setDivider(new ColorDrawable(android.R.color.transparent));
		swipeListView.setDividerHeight(0);
		// Endless scrolling...
		swipeListView.setOnScrollListener(new EndlessScrollListener());
	}

	private void setSLVchoiceModeAndMultiSelect() {
		swipeListView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			swipeListView
					.setMultiChoiceModeListener(new AbsListView.MultiChoiceModeListener() {

						private Set<Integer> s = new HashSet<Integer>();

						@Override
						public void onItemCheckedStateChanged(ActionMode mode,
								int position, long id, boolean checked) {
							mode.setTitle(swipeListView.getCountSelected()
									+ " seleccionado(s)");
							s.add((int) id);
						}

						@Override
						public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
							switch (item.getItemId()) {
							case R.id.menu_delete:
								swipeListView.dismissSelected();
								DAOItem d = new DAOItem();
								d.deleteItem(TimelineActivity.this, s);
								s.clear();
								mode.finish();
								return true;
							default:
								return false;
							}
						}

						@Override
						public boolean onCreateActionMode(ActionMode mode, Menu menu) {
							MenuInflater inflater = mode.getMenuInflater();
							inflater.inflate(R.menu.menu_choice_items, menu);
							return true;
						}

						@Override
						public void onDestroyActionMode(ActionMode mode) {
							swipeListView.unselectedChoiceStates();
						}

						@Override
						public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
							return false;
						}
					});
		}

		swipeListView.setSwipeListViewListener(new BaseSwipeListViewListener() {
			@Override
			public void onOpened(int position, boolean toRight) {
			}

			@Override
			public void onClosed(int position, boolean fromRight) {
			}

			@Override
			public void onListChanged() {
			}

			@Override
			public void onMove(int position, float x) {
			}

			@Override
			public void onStartOpen(int position, int action, boolean right) {
				if (Constants.SWIPELISTVIEW_DEBUG)
					Log.d("swipe",
							String.format("onStartOpen %d - action %d", position, action));
			}

			@Override
			public void onStartClose(int position, boolean right) {
				if (Constants.SWIPELISTVIEW_DEBUG)
					Log.d("swipe", String.format("onStartClose %d", position));
			}

			@Override
			public void onClickFrontView(int position) {
				if (Constants.SWIPELISTVIEW_DEBUG)
					Log.d("swipe", String.format("onClickFrontView %d", position));
			}

			@Override
			public void onClickBackView(int position) {
				if (Constants.SWIPELISTVIEW_DEBUG)
					Log.d("swipe", String.format("onClickBackView %d", position));
			}

			@Override
			public void onDismiss(int[] reverseSortedPositions) {
				Log.i(Constants.TAG, "swipeListView onDismiss() "
						+ reverseSortedPositions.length);
				Collection<Integer> intList = new HashSet<Integer>();
				for (int i : reverseSortedPositions)
					intList.add(i);
				adapter.deleteMultipleItems(TimelineActivity.this, intList);
				for (int position : reverseSortedPositions) {
					data.remove(position);
				}
				adapter.notifyDataSetChanged();
			}
		});
	}

	private void setupDrawerLayoutAndDrawerToggle() {
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_launcher, R.string.app_name, R.string.app_name) {

			/** Called when a drawer has settled in a completely closed state. */
			public void onDrawerClosed(View drawerView) {
				abTitle.setText(R.string.title_ab_main);
				if (drawerView.getId() == R.id.left_drawer) {
					abLeft.setImageResource(R.drawable.ic_menu_left_normal);
				} else if (drawerView.getId() == R.id.right_drawer) {
					abRight.setImageResource(R.drawable.ic_menu_right_normal);
				}
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			/** Called when a drawer has settled in a completely open state. */
			public void onDrawerOpened(View drawerView) {
				if (drawerView.getId() == R.id.left_drawer) {
					abTitle.setText(R.string.title_ab_left);
					abLeft.setImageResource(R.drawable.ic_menu_left_pressed);
				} else if (drawerView.getId() == R.id.right_drawer) {
					abTitle.setText(R.string.title_ab_right);
					abRight.setImageResource(R.drawable.ic_menu_right_pressed);
				}
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};

		// Set the drawer toggle as the DrawerListener
		mDrawerLayout.setDrawerListener(mDrawerToggle);
	}

	void loadCustomActionBar() {
		// Define custom action bar...
		RelativeLayout ll = new RelativeLayout(this);

		abTitle = new TextViewSanchez(this);
		abTitle.setText(getString(R.string.title_ab_main));
		abTitle.setTextColor(getResources().getColor(R.color.tv_ab_title));
		abTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 28);

		abLeft = new ImageView(this);
		abLeft.setImageResource(R.drawable.ic_menu_left_normal);
		abLeft.setClickable(true);
		abLeft.setScaleType(ScaleType.FIT_CENTER);
		abLeft.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// toggleLeft();
				new FakePopulateListTask(false).execute(true);
			}
		});

		abRight = new ImageView(this);
		abRight.setImageResource(R.drawable.ic_menu_right_normal);
		abRight.setClickable(true);
		abRight.setScaleType(ScaleType.FIT_CENTER);
		abRight.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				toggleRight();
			}
		});

		RelativeLayout.LayoutParams pL = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		pL.addRule(RelativeLayout.ALIGN_PARENT_LEFT, abLeft.getId());
		ll.addView(abLeft, pL);

		RelativeLayout.LayoutParams pR = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		pR.addRule(RelativeLayout.ALIGN_PARENT_RIGHT, abRight.getId());
		ll.addView(abRight, pR);

		RelativeLayout.LayoutParams pC = new RelativeLayout.LayoutParams(
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT);
		pC.addRule(RelativeLayout.CENTER_IN_PARENT, abTitle.getId());
		ll.addView(abTitle, pC);

		ll.setBackgroundColor(getResources().getColor(R.color.ab_background));

		getActionBar().setCustomView(ll);
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
	}

	public class EndlessScrollListener implements OnScrollListener {

		private int visibleThreshold = 4;
		private int previousTotal = 0;
		private boolean loading = true;

		public EndlessScrollListener() {
		}

		public EndlessScrollListener(int visibleThreshold) {
			this.visibleThreshold = visibleThreshold;
		}

		@Override
		public void onScroll(AbsListView view, int firstVisibleItem,
				int visibleItemCount, int totalItemCount) {

			if (loading) {
				if (totalItemCount > previousTotal) {
					loading = false;
					previousTotal = totalItemCount;
				}
			}
			if (!loading
					&& (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
				new PopulateListTask(false).execute();
				loading = true;
			}

			if (firstVisibleItem > mFirstVisibleItem) {
				toggleBottomVisibility(false);
			} else if (firstVisibleItem < mFirstVisibleItem) {
				toggleBottomVisibility(true);
			}
			mFirstVisibleItem = firstVisibleItem;
		}

		@Override
		public void onScrollStateChanged(AbsListView view, int scrollState) {
		}
	}

	public void onNewPostRequested(View v) {
		Intent intent = new Intent(this, CreateCommentActivity.class);
		startActivity(intent);
	}

	public void onScrollToTopRequested(View v) {
		Toast.makeText(this, "Scrolling up...", Toast.LENGTH_SHORT).show();
		// swipeListView.smoothScrollToPosition(0);
		swipeListView.setSelectionAfterHeaderView();
	}

	public void toggleBottomVisibility(boolean isScrollingUp) {

		if (mLastScrollingUp != isScrollingUp) {
			// Swipe direction changed
			final ImageView mListUp = (ImageView) findViewById(R.id.iv_list_up);
			final ImageView mListNew = (ImageView) findViewById(R.id.iv_list_new);

			if (isScrollingUp) {
				Animation anim = AnimationUtils.loadAnimation(this,
						R.anim.ic_btns_translate_disappear);
				anim.setFillAfter(true);
				mListUp.clearAnimation();
				mListNew.clearAnimation();
				mListUp.startAnimation(anim);
				mListNew.startAnimation(anim);
			} else {
				Animation anim = AnimationUtils.loadAnimation(this,
						R.anim.ic_btns_translate_appear);
				anim.setFillAfter(true);
				findViewById(R.id.iv_list_new).setAnimation(anim);
				findViewById(R.id.iv_list_up).setAnimation(anim);
				mListUp.clearAnimation();
				mListNew.clearAnimation();
				mListUp.startAnimation(anim);
				mListNew.startAnimation(anim);
			}
		}
		mLastScrollingUp = isScrollingUp;
	}
}

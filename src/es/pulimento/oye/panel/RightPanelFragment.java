package es.pulimento.oye.panel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import es.pulimento.oye.R;
import es.pulimento.oye.TimelineActivity;
import es.pulimento.oye.summary.SummaryActActivity;
import es.pulimento.oye.summary.SummaryCallActivity;
import es.pulimento.oye.summary.SummaryMusicActivity;
import es.pulimento.oye.summary.SummaryPositionActivity;
import es.pulimento.oye.summary.SummaryTwitterActivity;
import es.pulimento.oye.thirdpartyapis.twitter.TwitterHandlerActivity;

public class RightPanelFragment extends Fragment implements OnClickListener {

	public RightPanelFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_panel_right,
				container, false);

		return rootView;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setOnClickListeners();
	}

	@Override
	public void onClick(View v) {
		Intent intent;
		switch (v.getId()) {

		case R.id.btn_rmenu_twitter_stats:
			intent = new Intent(getActivity(), SummaryTwitterActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_rmenu_twitter_login:
			intent = new Intent(getActivity(), TwitterHandlerActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_rmenu_activity_stats:
			intent = new Intent(getActivity(), SummaryActActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_rmenu_call_stats:
			intent = new Intent(getActivity(), SummaryCallActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_rmenu_music_stats:
			intent = new Intent(getActivity(), SummaryMusicActivity.class);
			startActivity(intent);
			break;
		case R.id.btn_rmenu_position_stats:
			intent = new Intent(getActivity(), SummaryPositionActivity.class);
			startActivity(intent);
			break;
		}
		((TimelineActivity) getActivity()).toggleRight();
	}

	void setOnClickListeners() {
		getActivity().findViewById(R.id.btn_rmenu_twitter_stats)
				.setOnClickListener(this);
		getActivity().findViewById(R.id.btn_rmenu_twitter_login)
				.setOnClickListener(this);
		getActivity().findViewById(R.id.btn_rmenu_activity_stats)
				.setOnClickListener(this);
		getActivity().findViewById(R.id.btn_rmenu_call_stats)
				.setOnClickListener(this);
		getActivity().findViewById(R.id.btn_rmenu_music_stats)
				.setOnClickListener(this);
		getActivity().findViewById(R.id.btn_rmenu_position_stats)
				.setOnClickListener(this);
	}

}

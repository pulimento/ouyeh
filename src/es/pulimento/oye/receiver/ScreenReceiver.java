package es.pulimento.oye.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import es.pulimento.oye.misc.BaseReceiver;

public class ScreenReceiver extends BaseReceiver {

	private static final String OWN_NAME = "Screen on/off";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();

		if (null == bundle)
			return;

		Boolean wasScreenOn = null;
		if (intent.getAction().equals(Intent.ACTION_SCREEN_OFF)) {
			wasScreenOn = false;
		} else if (intent.getAction().equals(Intent.ACTION_SCREEN_ON)) {
			wasScreenOn = true;
		}

		potter(context, intent, OWN_NAME, true, String.valueOf(wasScreenOn));
	}
}
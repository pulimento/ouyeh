package es.pulimento.oye.db;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import es.pulimento.oye.misc.Constants;

public class MockTable {

	// Database table
	public static final String TABLE_MOCK = "mock";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_TIMESTAMP = "timestamp";
	public static final String COLUMN_INTENT = "intent";
	public static final String COLUMN_DATA_1 = "data1";
	public static final String COLUMN_DATA_2 = "data2";

	// Database creation SQLite statement
	public static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS "
			+ TABLE_MOCK + "(" + COLUMN_ID
			+ " integer primary key autoincrement," + COLUMN_TIMESTAMP
			+ " integer, " + COLUMN_INTENT + " text, " + COLUMN_DATA_1
			+ " text," + COLUMN_DATA_2 + " text);";

	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	public static void onUpgrade(SQLiteDatabase database, int oldVersion,
			int newVersion) {
		Log.w(Constants.TAG, "Upgrading database from version " + oldVersion
				+ " to " + newVersion + ", which will destroy all old data");
		database.execSQL("DROP TABLE IF EXISTS " + TABLE_MOCK);
		onCreate(database);
	}

}

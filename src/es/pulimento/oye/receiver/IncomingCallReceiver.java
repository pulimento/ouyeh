package es.pulimento.oye.receiver;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import es.pulimento.oye.misc.BaseReceiver;

public class IncomingCallReceiver extends BaseReceiver {

	private static final String OWN_NAME = "IncomingCall";

	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();

		if (null == bundle) {
			return;
		}

		String state = bundle.getString(TelephonyManager.EXTRA_STATE);
		String phoneNumber = "<PHONE NUMBER>";
		if (state.equalsIgnoreCase(TelephonyManager.EXTRA_STATE_RINGING)) {
			phoneNumber = bundle
					.getString(TelephonyManager.EXTRA_INCOMING_NUMBER);
		}

		potter(context, intent, OWN_NAME, true, phoneNumber);
	}
}
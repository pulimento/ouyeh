package es.pulimento.oye.bean;

import es.pulimento.oye.misc.Constants;

public class CommentItem extends ItemEvent {

	private int _id;
	private String text;
	private String imageName;
	private String misc;
	public static final int type = Constants.TYPE_COMMENT;

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getImageName() {
		return imageName;
	}

	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	public String getMisc() {
		return misc;
	}

	public void setMisc(String misc) {
		this.misc = misc;
	}

	@Override
	public String getField1() {
		return text;

	}

	@Override
	public String getField2() {

		return misc;
	}

	@Override
	public String getField3() {
		return imageName;
	}

	@Override
	public int getType() {
		return type;
	}

}

package es.pulimento.oye.thirdpartyapis.twitter;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Window;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import es.pulimento.oye.R;

public class TwitterSignInActivity extends Activity {

	private Intent mIntent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_twitter_sign_in);
		mIntent = getIntent();
		String url = (String) mIntent.getExtras().get("URL");
		WebView webView = (WebView) findViewById(R.id.twitter_sign_in_webview);
		WebSettings mWebSettings = webView.getSettings();
		// This method was deprecated in API level 18.
		// Saving passwords in WebView will not be supported in future versions.
		// So, it's OK to use it here
		mWebSettings.setSavePassword(false);
		mWebSettings.setBuiltInZoomControls(false);
		mWebSettings.setGeolocationEnabled(false);
		mWebSettings.setSaveFormData(false);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				if (url.contains(getString(R.string.twitter_callback_oye))) {
					Uri uri = Uri.parse(url);
					String oauthVerifier = uri
							.getQueryParameter("oauth_verifier");
					mIntent.putExtra("oauth_verifier", oauthVerifier);
					setResult(RESULT_OK, mIntent);
					finish();
					return true;
				}
				return false;
			}
		});
		webView.loadUrl(url);
	}
}

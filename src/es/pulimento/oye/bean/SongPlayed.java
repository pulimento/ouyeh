package es.pulimento.oye.bean;

import es.pulimento.oye.misc.Constants;

public class SongPlayed extends ItemEvent {

	private int _id;
	private String songTitle;
	private String songArtist;
	private String songAlbum;
	private String misc;
	private String coverURL;
	public final int type = Constants.TYPE_SONG_PLAYED;

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getSongTitle() {
		return songTitle;
	}

	public void setSongTitle(String songTitle) {
		this.songTitle = songTitle;
	}

	public String getSongArtist() {
		return songArtist;
	}

	public void setSongArtist(String songArtist) {
		this.songArtist = songArtist;
	}

	public String getSongAlbum() {
		return songAlbum;
	}

	public void setSongAlbum(String songAlbum) {
		this.songAlbum = songAlbum;
	}

	public String getMisc() {
		return misc;
	}

	public void setMisc(String misc) {
		this.misc = misc;
	}

	public String getCoverURL() {
		return coverURL;
	}

	public void setCoverURL(String coverURL) {
		this.coverURL = coverURL;
	}

	@Override
	public String getField1() {
		return songTitle;
	}

	@Override
	public String getField2() {
		return songArtist;
	}

	@Override
	public String getField3() {
		return songAlbum;
	}

	@Override
	public int getType() {
		return type;
	}

}

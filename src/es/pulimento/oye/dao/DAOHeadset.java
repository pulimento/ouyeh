package es.pulimento.oye.dao;

import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import es.pulimento.oye.db.HeadsetPlugTable;
import es.pulimento.oye.db.ItemTable;
import es.pulimento.oye.db.MainDatabaseHelper;
import es.pulimento.oye.misc.Constants;

public class DAOHeadset {

	public DAOHeadset() {
	}

	public Future<Long> insert(final Context context, final ContentValues values) {

		Callable<Long> c = new Callable<Long>() {

			@Override
			public Long call() throws Exception {
				long id = -1;

				ContentValues cvItem = new ContentValues();
				cvItem.put(ItemTable.COLUMN_TIMESTAMP,
						System.currentTimeMillis());
				cvItem.put(ItemTable.COLUMN_TYPE, Constants.TYPE_HEADSET_PLUG);

				MainDatabaseHelper dbHelper = MainDatabaseHelper
						.getInstance(context);
				SQLiteDatabase db = dbHelper.getWritableDatabase();

				//db.beginTransaction();
				long idItem = db
						.insert(ItemTable.TABLE_NAME, "<empty>", cvItem);
				values.put(HeadsetPlugTable.COLUMN_ID_ITEM, idItem);
				id = db.insert(HeadsetPlugTable.TABLE_NAME, "<empty>", values);
				//db.setTransactionSuccessful();
				//db.endTransaction();
				//db.close();

				return id;
			};
		};
		ScheduledExecutorService s = Executors
				.newSingleThreadScheduledExecutor();
		return s.submit(c);
	}

}

Se presenta el proyecto final de carrera

Proyecto: Registro de información contextual en dispositivos móviles
Alumno: Francisco Javier Pulido Espina, 44244157T
Titulación: Ingeniería técnica en Informática de Gestión
Tutor: D. Luis Miguel Soria Morillo
Departamento de Lenguajes y Sistemas Informáticos

Se presentan dos archivos, uno en la carpeta "código", que contiene la implementación
del proyecto, desarrollado en Eclipse Juno; y otro en la carpeta "memoria", que contiene
la memoria realizada durante el desarrollo del proyecto



Sevilla, Septiembre de 2013
package es.pulimento.oye.misc.customview;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewQtr extends TextView {

	public TextViewQtr(Context context) {
		super(context);
		useCustomTypography();
	}

	public TextViewQtr(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		useCustomTypography();
	}

	public TextViewQtr(Context context, AttributeSet attrs) {
		super(context, attrs);
		useCustomTypography();
	}

	private void useCustomTypography() {
		Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
				"fonts/QuattrocentoSans-Regular.ttf");
		setTypeface(tf, 1);
	}
}

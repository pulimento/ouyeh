package es.pulimento.oye.fetcher;

import java.sql.Time;
import java.util.Random;

import android.util.Log;
import es.pulimento.oye.bean.GeneratedActivity;
import es.pulimento.oye.misc.Constants;

public class ActivityGenerator {

	public static String[] ACTIVITIES_8_TO_14 = { "desayunando", "en clases",
			"en la facultad", "tomando algo a media ma�ana", "estudiando",
			"programando" };
	public static String[] ACTIVITIES_14_TO_20 = { "corriendo", "entrenando",
			"almorzando", "merendando", "tomando cerveza", "de compras",
			"en el parque", "jugando al f�tbol", "programando" };
	public static String[] ACTIVITIES_20_TO_2 = { "en un bar", "en el cine",
			"de paseo", "tomando unas tapas", "cenando", "paseando",
			"tomando un helado", "programando" };
	public static String[] ACTIVITIES_2_TO_8 = { "durmiendo",
			"planchando la oreja", "de fiesta", "programando", "descansando" };

	public static String[] PEOPLE = { "Luis Cruz", "Luis Salas", "Paco Gand�a",
			"Ana Bellido", "Juan Cort�s", "Mar�a Bahamonde", "Elena Delgado",
			"Paco Tous" };

	public static GeneratedActivity generate() {
		Random r = new Random(System.currentTimeMillis() * 31);
		String people = PEOPLE[r.nextInt(PEOPLE.length)];
		String activity = null;

		GeneratedActivity act = new GeneratedActivity();

		act.setName(null);
		act.setPerson(people);

		Time t = new Time(System.currentTimeMillis());
		if (t.getHours() > 8 && t.getHours() <= 14) {
			activity = ACTIVITIES_8_TO_14[r.nextInt(ACTIVITIES_8_TO_14.length)];
		} else if (t.getHours() > 14 && t.getHours() <= 20) {
			activity = ACTIVITIES_14_TO_20[r
					.nextInt(ACTIVITIES_14_TO_20.length)];
		} else if (t.getHours() > 20 && t.getHours() <= 2) {
			activity = ACTIVITIES_20_TO_2[r.nextInt(ACTIVITIES_20_TO_2.length)];
		} else if (t.getHours() > 2 && t.getHours() <= 8) {
			activity = ACTIVITIES_2_TO_8[r.nextInt(ACTIVITIES_2_TO_8.length)];
			act.setPerson(null);
		}

		act.setActivity(activity);

		Log.i(Constants.TAG, "Activity generated!! " + activity + " " + people);

		return act;
	}

}
